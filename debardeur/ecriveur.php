<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/debardeur_depots');

/**
 * @param array $liste_sources
 * @param bool $force
 * @param string $depot
 * @return bool
 * @throws Exception
 */
function debardeur_ecrire($liste_sources, $depot, $dir_sources=null, $dir_depots = null) {
	include_spip('inc/debardeur');
	debardeur_init();

	if (is_null($dir_sources)) {
		$dir_sources = _DIR_DEBARDEUR_SOURCES;
	}
	debardeur_check_dir($dir_sources);

	if (is_null($dir_depots)) {
		$dir_depots = _DIR_DEBARDEUR_DEPOTS;
	}
	debardeur_check_dir($dir_depots);

	$dir_depot = debardeur_depot_preparer($dir_depots, $depot);
	if (count($liste_sources)>1) {
		debardeur_log("<comment>Ajouter les zips au dépot $depot $dir_depot</comment>");
		debardeur_log(' ');
	}
	else {
		debardeur_log("Ajouter les zips au dépot $depot $dir_depot");
	}

	$total_ajout = 0;
	foreach ($liste_sources as $source){

		debardeur_log("<info>--- Source " . $source['short'] . " | " . $source['slug'] . " | " . $source['url']."</info>");

		$dir_source = $dir_sources . $source['dir_checkout'] . '/';

		$nb_files = debardeur_depot_ajouter_source($dir_depot, $dir_source, $source);
		debardeur_log($nb_files ? " - $nb_files zips ajoutés au dépot $depot" : " - Rien a faire");
		$total_ajout += $nb_files;
	}

	// on fait pas de comptage dans l'ecriveur quand il y a une seule source et qu'on a rien ajoute (verbosite des crons)
	if (count($liste_sources) > 1 or $total_ajout>0) {
		$dir = getcwd();
		chdir(dirname($dir_depot));

		$out = "";
		$nb = debardeur_depot_compter_zips(basename($dir_depot), $out);
		debardeur_log("\n<comment>$nb zips dans le dépot $depot</comment>");
		debardeur_log($out);

		chdir($dir);
	}

	return true;
}
