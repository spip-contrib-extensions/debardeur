<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * @param array $depots
 * @param string $dir_depots
 * @return bool
 * @throws Exception
 */
function debardeur_compiler($depots, $dir_depots = null) {

	include_spip('inc/debardeur');
	include_spip('inc/debardeur_depots');

	debardeur_init();

	if (is_null($dir_depots)) {
		$dir_depots = _DIR_DEBARDEUR_DEPOTS;
	}
	debardeur_check_dir($dir_depots);

	foreach($depots as $depot) {
		$dir_depot = debardeur_depot_preparer($dir_depots, $depot);
		debardeur_log("<comment>Générer le archives.xml du dépot $depot $dir_depot</comment>");

		list($nb_paquets, $cleaned) = debardeur_depot_compiler($dir_depot);
		if ($nb_cleaned = count($cleaned)) {
			debardeur_log("\n$nb_cleaned fichiers supprimés du dépot $depot : " . implode(', ',$cleaned));
		}
		debardeur_log("\n<comment>$nb_paquets paquets dans le dépot $depot</comment>");

		$dir = getcwd();
		chdir(dirname($dir_depot));
		$out = "";
		$nb_zips = debardeur_depot_compter_zips(basename($dir_depot), $out);
		if ($nb_zips == $nb_paquets) {
			debardeur_log($out);
		}
		chdir($dir);

	}

	return true;
}
