<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/debardeur_analyser_zip');

/**
 * @param array $liste_sources
 * @param bool $force
 * @param string|null $tmp
 * @return bool
 * @throws Exception
 */
function debardeur_lire($liste_sources, $force = false, $dir_sources=null) {
	include_spip('inc/debardeur');
	debardeur_init();

	if (is_null($dir_sources)) {
		$dir_sources = _DIR_DEBARDEUR_SOURCES;
	}
	debardeur_check_dir($dir_sources);

	foreach ($liste_sources as $source){

		debardeur_log("<info>--- Source " . $source['short'] . " | " . $source['slug'] . " | " . $source['url']."</info>");

		$dir_source = $dir_sources . $source['dir_checkout'] . '/';
		$zip_files = glob($dir_source . "*.zip");

		$show_detail = false;
		$log_wait = [];
		foreach ($zip_files as $zip_file) {
			$res = debardeur_analyser_zip($zip_file, $source['url'], $force);
			if ($res) {
				// si il y a un traitement on montre le detail, sinon un resume
				$show_detail = true;
			}
			$out = "  |- " . ($res ? $res : "$zip_file OK");
			if ($show_detail) {
				while(count($log_wait)) {
					debardeur_log(array_shift($log_wait));
				}
				debardeur_log($out);
			}
			else {
				$log_wait[]	= $out;
			}
		}
		if (!$show_detail) {
			debardeur_log("  |- " . count($zip_files) . " zips déjà traités (rien à faire)");
		}
	}

	return true;
}

