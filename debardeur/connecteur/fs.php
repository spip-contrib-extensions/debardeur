<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Recuperer les tags (=zip) depuis une source File System
 * permet de choper les zips de smart-paquet
 * @param string $dir_fs
 *   le chemin sur le file system : par securite c'est un sous-dossier de debardeur/sources_fs/ (peut etre un lien symbolique)
 * @param string $dir_source
 * @param bool $force_update
 * @return array|string
 */
function debardeur_connecteur_fs_recuperer_tags($dir_fs, $dir_source, $force_update = false) {
	if (!is_dir($dir_source)) {
		passthru("mkdir -p $dir_source");
	}

	if (!defined('_DIR_DEBARDEUR_SOURCES_FS')){
		define('_DIR_DEBARDEUR_SOURCES_FS', _DIR_DEBARDEUR . 'sources_fs/');
	}
	debardeur_check_dir(_DIR_DEBARDEUR_SOURCES_FS);

	$dir_fs = trim($dir_fs, '/');
	$dir_fs = preg_replace(",[^\w/-],", "_", $dir_fs);

	$dir_fs = _DIR_DEBARDEUR_SOURCES_FS . $dir_fs . '/';
	debardeur_check_dir($dir_fs);

	// on liste tous les zips du repertoire
	$fs_zip_files = glob($dir_fs . '*.zip');

	$tags = array();
	foreach ($fs_zip_files as $fs_zip_file) {

		$tag_name = basename($fs_zip_file, '.zip');
		$zip_base_name = basename($fs_zip_file);
		$zip_file = $dir_source . $zip_base_name;

		$new = false;
		if (!file_exists($zip_file)
		  or filemtime($zip_file) < filemtime($fs_zip_file)) {
			$new = true;
			copy($fs_zip_file, $zip_file);
			touch($zip_file, filemtime($fs_zip_file));

			if (!filesize($zip_file)) {
				return "Echec creation fichier $zip_file";
			}
		}

		$tags[$tag_name] = [
			'new' => $new,
			'zip' => $zip_file
		];
	}

	debardeur_nettoyer_source($dir_source, $tags);

	return $tags;
}