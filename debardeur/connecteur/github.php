<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Lister les tags d'un repository
 *
 * @param string $url_repository
 * @param string $dir_source
 * @param bool $force_update
 * @return array|string
 */
function debardeur_connecteur_github_recuperer_tags($url_repository, $dir_source, $force_update = false) {
	if (!is_dir($dir_source)) {
		passthru("mkdir -p $dir_source");
	}

	$dir_source = rtrim($dir_source,'/') . '/';

	$endpoint = github_endpoint_from_url($url_repository);
	$repository = github_repository_from_url($url_repository);
	$org = github_organisation_from_url($url_repository);

	// d'abord recuperer le last-modified du repo depuis une liste en cache
	$url_organisation = explode("/$org/", $url_repository);
	$url_organisation = reset($url_organisation) . "/$org";

	$last_modified = null;
	$repositories = debardeur_connecteur_github_lister_repositories($url_organisation, strtotime('-1hour'));
	if (isset($repositories[$url_repository])) {
		$last_modified = $repositories[$url_repository]['last_modified'];
	}

	// lister les tags
	$liste_tags = github_get_tags($endpoint, $repository, $last_modified);
	if ($liste_tags === false) {
		return "Echec recuperation tags sur Github $endpoint pour projet $repository";
	}

	$tags = array();
	foreach ($liste_tags as $tag_name => $tag_detail) {
		// forcer l'extension .zip si elle n'y est pas
		$zip_file = $dir_source . basename($tag_detail['zipball_url'],'.zip') . ".zip";

		$new = false;
		if (!file_exists($zip_file)) {
			$new = true;
			// on curl le zip
			passthru("curl --silent -L ".escapeshellarg($tag_detail['zipball_url'])." > $zip_file");
			if (!file_exists($zip_file)) {
				return "Echec recuperation zip ".$tag_detail['zipball_url'];
			}
			if ($timestamp = github_get_commit_timestamp($endpoint, $repository, $tag_detail['sha'])) {
				// mettre le zip a la date du tag
				touch($zip_file, $timestamp);
			}
		}

		$tags[$tag_name] = [
			'new' => $new,
			'zip' => $zip_file
		];
	}

	debardeur_nettoyer_source($dir_source, $tags);

	return $tags;
}


/**
 * Lister les repositories d'une organisation
 * https://api.github.com/orgs/spip/repos
 *
 * @param string $url_organisation
 * @param int $last_modified_time
 * @return array
 */
function debardeur_connecteur_github_lister_repositories($url_organisation, $last_modified_time = null) {
	$endpoint = github_endpoint_from_url($url_organisation);
	$org = github_organisation_from_url($url_organisation);

	$method = "orgs/{$org}/repos";
	$res = debardeur_json_api_call('github', $endpoint, $method, '', $last_modified_time);

	$repositories = [];
	if ($res) {
		foreach ($res as $row) {
			if (is_array($row)) {
				$repositories[$row['clone_url']] = [
					'name' => $row['name'],
					'full_name' => $row['full_name'],
					'url' => $row['clone_url'],
					'last_modified' => strtotime($row['updated_at'])
				];
			}
		}
	}

	return $repositories;
}

/****************************************/


function github_endpoint_from_url($url){
	// un seul github
	$endpoint = "https://api.github.com/";

	return $endpoint;
}


function github_repository_from_url($url){
	$url = explode('://', $url, 2);
	$path = explode("/", end($url), 2);
	$path = end($path);
	$repository = preg_replace(",\.git$,", "", $path);

	return $repository;
}


function github_organisation_from_url($url){
	$repository = github_repository_from_url($url);
	$repository = explode('/', $repository);
	$organisation = reset($repository);

	return $organisation;
}


/**
 * Recuperer les tags d'un projet github
 * https://api.github.com/repos/spip/SPIP/tags
 *
 * @param string $endpoint
 * @param string $repository
 * @return bool|array
 */
function github_get_tags($endpoint, $repository, $last_modified=null) {

	$method = "repos/" . $repository . "/tags";
	$liste = debardeur_json_api_call('github', $endpoint, $method, '', $last_modified);
	if ($liste === false) {
		return false;
	}

	$tags = [];
	foreach($liste as $t) {
		if (!isset($t['zipball_url'])) {
			return false;
		}
		$tag = [
			'sha' => $t['commit']['sha'],
			'zipball_url' => $t['zipball_url'],
			'tarball_url' => $t['tarball_url'],
		];
		$tags[$t['name']] = $tag;
	}

	return $tags;
}


/**
 * Retrouve le timestamp d'un commit
 * https://api.github.com/repos/spip/SPIP/git/commits/72b09a322c764e10a68b4c26d6ba3d5262b6776e
 *
 * @param $endpoint
 * @param $repository
 * @param $sha
 * @return bool|int
 */
function github_get_commit_timestamp($endpoint, $repository, $sha) {
	$method = "repos/" . $repository . "/git/commits/$sha";
	$infos = debardeur_json_api_call('github', $endpoint, $method);
	if (!$infos) {
		return false;
	}

	// attention, ici on est pas identique a github : pas de sous tableau 'commit'
	$date = '';
	if (isset($infos['committer']['date'])) {
		$date = $infos['committer']['date'];
	}
	elseif (isset($infos['author']['date'])) {
		$date = $infos['author']['date'];
	}
	return $date ? strtotime($date) : 0;
}
