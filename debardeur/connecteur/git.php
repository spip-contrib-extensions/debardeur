<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Recuperer les tags (=zip) depuis une source git
 * @param string $url
 * @param string $dir_source
 * @param bool $force_update
 * @return array|string
 */
function debardeur_connecteur_git_recuperer_tags($url, $dir_source, $force_update = false) {
	if (!is_dir($dir_source)) {
		passthru("mkdir -p $dir_source");
	}

	$short_base_name = basename($url,'.git');

	$dir_source = rtrim($dir_source,'/') . '/';
	// on fait un checkout de l'url dans un sous-dossier checkout
	$dir_checkout = $dir_source . $short_base_name;

	$url_with_credentials = debardeur_set_credentials('git', $url);

	$file_log = $dir_source . "checkout.log";
	$cmd = "checkout.php git -bmaster $url_with_credentials $dir_checkout 2>&1 1> $file_log";
	file_put_contents($file_log, "$cmd\n");
	passthru("export FORCE_RM_AND_CHECKOUT_AGAIN_BAD_DEST=1 && $cmd 2>/dev/null", $return);

	if ($return !== 0 or !is_dir($dir_checkout)) {
		return "Echec checkout `$cmd`";
	}

	// lister les tags
	$liste = [];
	exec("cd $dir_checkout && git tag -l", $liste);
	$liste = array_map('trim', $liste);
	$liste = array_filter($liste);

	$tags = array();
	foreach ($liste as $tag_name) {
		$zip_name = preg_replace(",[^\w.-],", "_", $tag_name);
		$zip_file = $dir_source . $zip_name . ".zip";

		$new = false;
		if (!file_exists($zip_file) or !filesize($zip_file)) {
			$new = true;
			$short_zip_file = basename($zip_file);
			// construire le zip depuis le checkout
			passthru("cd $dir_checkout && git checkout $tag_name");
			$lastmodified = debardeur_conecteur_git_lastmodified_file_dist($dir_checkout, '.');
			passthru("cd $dir_source && zip -roXq $short_zip_file $short_base_name -x \*/.git\*");

			if (!filesize($zip_file)) {
				return "Echec creation fichier $zip_file";
			}
			// mettre le zip a la date du dernier commit
			touch($zip_file, $lastmodified);
		}

		$tags[$tag_name] = [
			'new' => $new,
			'zip' => $zip_file
		];
	}

	debardeur_nettoyer_source($dir_source, $tags);

	return $tags;
}

/**
 * Formate l'auteur en Nom <email> si jamais seul l'email est fourni
 * @param string $author
 * @return string
 */
function debardeur_git_format_author($author) {
	if (strpos($author, '<') !== false and strpos($author, '>') !== false) {
		return $author;
	}
	else {
		$name = explode('@', $author);
		$name = reset($name);
		return "$name <$author>";
	}
}

/**
 * Lire la date de derniere modif d'un fichier versionne
 * (retourne 0 si le fichier n'est pas versionne)
 * @param string $dir_repo
 * @param string $file
 * @return false|int
 */
function debardeur_conecteur_git_lastmodified_file_dist($dir_repo, $file) {

	$d = getcwd();
	chdir($dir_repo);
	$file = escapeshellarg($file);
	$lastmodified = exec("git log -1 -c --pretty=tformat:'%ct' $file | head -1");
	$lastmodified = intval(trim($lastmodified));
	chdir($d);
	return $lastmodified;
}

/**
 * Afficher le status d'un ou plusieurs fichiers
 * @param string $dir_repo
 * @param string|array $file_or_files
 * @return string
 */
function debardeur_conecteur_git_status_file_dist($dir_repo, $file_or_files) {

	if (is_array($file_or_files)) {
		$file_or_files = array_map('escapeshellarg', $file_or_files);
		$file_or_files = implode(' ', $file_or_files);
	}
	else {
		$file_or_files = escapeshellarg($file_or_files);
	}

	$d = getcwd();
	chdir($dir_repo);
	$output = array();
	exec("git status --short $file_or_files 2>&1", $output);
	//exec("svn status $files_list 2>&1", $output);
	chdir($d);
	return implode("\n", $output);
}
