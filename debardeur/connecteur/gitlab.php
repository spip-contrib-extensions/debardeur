<?php

/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Lister les tags d'un repository
 *
 * @param string $url_repository
 * @param string $dir_source
 * @param bool $force_update
 * @return array|string
 */
function debardeur_connecteur_gitlab_recuperer_tags($url_repository, $dir_source, $force_update = false) {
	if (!is_dir($dir_source)) {
		passthru("mkdir -p $dir_source");
	}

	$dir_source = rtrim($dir_source, '/') . '/';

	$endpoint = gitlab_endpoint_from_url($url_repository);
	$repository = gitlab_repository_slug_from_url($url_repository);
	$org = gitlab_organisation_from_url($url_repository);

	// d'abord recuperer le last-modified du repo depuis une liste en cache
	$url_organisation = explode("/$org/", $url_repository);
	$url_organisation = reset($url_organisation) . "/$org";

	$last_modified = null;
	if (!$force_update) {
		$repositories = debardeur_connecteur_gitlab_lister_repositories($url_organisation, strtotime('-30min'));
		if ($repositories and isset($repositories[$url_repository])) {
			$last_modified = $repositories[$url_repository]['last_modified'];
			// mais de toute facon on recheck toutes les +/- 12h-36h pour eviter que tous les paquets check en meme temps
			$last_modified = max($last_modified, time() - rand(12 * 3600, 36 * 3600));
		}
	}

	// lister les tags
	$project_id = gitlab_get_project_id($url_repository);
	if (!$project_id) {
		return "Echec Impossible de trouver l'id Gitlab du repository $url_repository";
	}
	$liste_tags = gitlab_get_tags($endpoint, $project_id, $last_modified);
	if ($liste_tags === false) {
		return "Echec recuperation tags sur Gitlab $endpoint pour projet $repository";
	}

	$tags = [];
	foreach ($liste_tags as $tag_name => $tag_detail) {
		// on filtre les tags a la source, en ne retenant que ceux formattes en v1.2.3 ou 1.2.3 (ou optionnellent v1.2/1.2 ou v1/1)
		// on accepte eventuellement un 4e digit
		if (preg_match(',^v?(\d+)(\.(\d+))?(\.(\d+))?(\.(\d+))?(-(alpha|beta|dev|rc).*)?$,i', $tag_name)) {
			// gerer les malvenus / dans les noms de tags : on remplace par un -
			if (strpos($tag_name, '/') !== false) {
				$tag_name_clean = str_replace('/', '-', $tag_name);
				$zip_file = str_replace($tag_name, $tag_name_clean, $tag_detail['zipball_url']);
				$zip_file = basename($zip_file);
			}
			else {
				$zip_file = basename($tag_detail['zipball_url']);
			}
			$zip_file = $dir_source . $zip_file;

			$new = false;
			// supprimer le zip existant si il est pas a jour (tag supprimé et remis...)
			if (file_exists($zip_file)
				and $t = filemtime($zip_file)
				and !empty($tag_detail['timestamp'])
				and $t < $tag_detail['timestamp']) {
				@unlink($zip_file);
			}
			// downloader le zip si besoin
			if (!file_exists($zip_file) or !filesize($zip_file)) {
				$new = true;
				// on curl le zip
				passthru('curl --silent -L ' . escapeshellarg($tag_detail['zipball_url']) . " > $zip_file");
				if (!file_exists($zip_file)) {
					return 'Echec recuperation zip ' . $tag_detail['zipball_url'];
				}
				if (!empty($tag_detail['timestamp'])) {
					// mettre le zip a la date du tag
					touch($zip_file, $tag_detail['timestamp']);
				}
			}

			$tags[$tag_name] = [
				'new' => $new,
				'zip' => $zip_file
			];
		}
	}

	debardeur_nettoyer_source($dir_source, $tags);

	return $tags;
}

/**
 * Lister les repositories d'une organisation
 * @param string $url_organisation
 * @param int $last_modified_time
 * @return array
 */
function debardeur_connecteur_gitlab_lister_repositories($url_organisation, $last_modified_time = null) {
	$endpoint = gitlab_endpoint_from_url($url_organisation);

	$org_id = gitlab_get_userid_organisation($url_organisation);

	$method = "groups/{$org_id}/projects";
	$res = debardeur_json_api_call_pages(0, 'gitlab', $endpoint, $method, ['per_page' => 100], $last_modified_time);

	$repositories = [];
	if ($res) {
		foreach ($res as $k => $row) {
			if ($k === 'message' and is_string($row)) {
				debardeur_fail('Echec API Gitlab', implode("\n", [$url_organisation, "API Call $endpoint $method", $row, json_encode($res)]));
			}
			if (
				is_array($row)
				and !empty($row['http_url_to_repo'])
				and empty($row['empty_repo'])
			) {
				$repositories[$row['http_url_to_repo']] = [
					'id' => $row['id'],
					'name' => $row['name'],
					'full_name' => $row['path_with_namespace'],
					'url' => $row['http_url_to_repo'],
					'last_modified' => strtotime($row['updated_at'])
				];
			}
		}
	}

	return $repositories;
}

/****************************************/


function gitlab_endpoint_from_url($url) {
	$url = explode('://', $url, 2);
	$host = explode('/', end($url), 2);
	$endpoint = $url[0] . '://' . $host[0] . '/api/v4/';

	return $endpoint;
}


function gitlab_repository_slug_from_url($url) {
	$url = explode('://', $url, 2);
	$path = explode('/', end($url), 2);
	$path = end($path);
	$repository = preg_replace(',\.git$,', '', $path);

	return $repository;
}

function gitlab_organisation_from_url($url) {
	$repository = gitlab_repository_slug_from_url($url);
	$repository = explode('/', $repository);
	$organisation = reset($repository);

	return $organisation;
}

function gitlab_get_userid_organisation($url_organisation) {
	static $groups = null;
	if (is_null($groups)) {
		$endpoint = gitlab_endpoint_from_url($url_organisation);
		$groups = debardeur_json_api_call('gitlab', $endpoint, 'groups');
	}
	$organisation_name = gitlab_organisation_from_url($url_organisation);
	foreach ($groups as $group) {
		if ($group['path'] === $organisation_name) {
			return $group['id'];
		}
	}
}

function gitlab_get_project_id($url_repository) {
	$organisation = gitlab_organisation_from_url($url_repository);
	$url = explode('://', $url_repository, 2);
	$host = explode('/', end($url), 2);
	$url_organisation = reset($url) . '://' . reset($host) . '/' . $organisation;
	$repositories = debardeur_connecteur_gitlab_lister_repositories($url_organisation, strtotime('-1hour'));
	if (isset($repositories[$url_repository])) {
		return $repositories[$url_repository]['id'];
	}
	$slug = gitlab_repository_slug_from_url($url_repository);
	foreach ($repositories as $repository) {
		if ($repository['full_name'] === $slug) {
			return $repository['id'];
		}
	}

	return 0;
}

/**
 * Recuperer les tags d'un projet gitlab
 * @param string $endpoint
 * @param string $repository_slug
 * @return bool|array
 */
function gitlab_get_tags($endpoint, $project_id, $last_modified = null) {

	$method = "/projects/{$project_id}/repository/tags";
	$liste = debardeur_json_api_call_pages(1, 'gitlab', $endpoint, $method, ['per_page' => 100], $last_modified);
	if ($liste === false or !is_array($liste)) {
		if ($liste) {
			spip_log('gitlab_get_tags: retour innatendu ' . json_encode($liste), 'debardeur');
		}
		return false;
	}

	/*
	[
	    {
	        "name": "v4.2.0",
	        "message": "",
	        "target": "a9c47e3881cfe916249fe91f790f23afefcaa7a6",
	        "commit": {
	            "id": "a9c47e3881cfe916249fe91f790f23afefcaa7a6",
	            "short_id": "a9c47e38",
	            "created_at": "2024-02-15T17:18:20.000+01:00",
	            "parent_ids": [
	                "0c0d8080012f5e2fc82312b5cb64d8e9cc304983"
	            ],
	            "title": "build: v4.2.0",
	            "message": "build: v4.2.0\n",
	            "author_name": "erational",
	            "author_email": "erational@erational.org",
	            "authored_date": "2024-02-15T17:18:20.000+01:00",
	            "committer_name": "erational",
	            "committer_email": "erational@erational.org",
	            "committed_date": "2024-02-15T17:18:20.000+01:00",
	            "trailers": {},
	            "extended_trailers": {},
	            "web_url": "https://git-lab.spip.net/spip-contrib-extensions/spip2spip/-/commit/a9c47e3881cfe916249fe91f790f23afefcaa7a6"
	        },
	        "release": null,
	        "protected": false
	    },
	...
	]
	*/

	$tags = [];
	foreach ($liste as $t) {
		if (empty($t['target']) or empty($t['commit']['web_url'])) {
			return false;
		}
		$base_url = $t['commit']['web_url'];
		$base_url = explode('/commit/', $base_url, 2);
		$base_url = reset($base_url);
		$tagname = $t['name'];
		$basename = basename($base_url);
		if ($basename === '-') {
			$basename = basename(dirname($base_url));
		}
		$xball_url = $base_url . "/archive/{$tagname}/{$basename}-{$tagname}";
		$tag = [
			'sha' => $t['target'],
			'zipball_url' => $xball_url . '.zip',
			'tarball_url' => $xball_url . '.tar.gz',
			'timestamp' => strtotime($t['commit']['committed_date'])
		];
		$tags[$tagname] = $tag;
	}

	return $tags;
}
