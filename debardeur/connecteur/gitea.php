<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Lister les tags d'un repository
 *
 * @param string $url_repository
 * @param string $dir_source
 * @param bool $force_update
 * @return array|string
 */
function debardeur_connecteur_gitea_recuperer_tags($url_repository, $dir_source, $force_update = false) {
	if (!is_dir($dir_source)) {
		passthru("mkdir -p $dir_source");
	}

	$dir_source = rtrim($dir_source,'/') . '/';

	$endpoint = gitea_endpoint_from_url($url_repository);
	$repository = gitea_repository_from_url($url_repository);
	$org = gitea_organisation_from_url($url_repository);

	// d'abord recuperer le last-modified du repo depuis une liste en cache
	$url_organisation = explode("/$org/", $url_repository);
	$url_organisation = reset($url_organisation) . "/$org";

	$last_modified = null;
	if (!$force_update) {
		$repositories = debardeur_connecteur_gitea_lister_repositories($url_organisation, strtotime('-30min'));
		if ($repositories and isset($repositories[$url_repository])) {
			$last_modified = $repositories[$url_repository]['last_modified'];
			// mais de toute facon on recheck toutes les +/- 12h-36h pour eviter que tous les paquets check en meme temps
			$last_modified = max($last_modified, time() - rand(12 * 3600, 36 * 3600));
		}
	}

	// lister les tags
	$liste_tags = gitea_get_tags($endpoint, $repository, $last_modified);
	if ($liste_tags === false) {
		return "Echec recuperation tags sur Gitea $endpoint pour projet $repository";
	}

	$tags = array();
	foreach ($liste_tags as $tag_name => $tag_detail) {
		// on filtre les tags a la source, en ne retenant que ceux formattes en v1.2.3 ou 1.2.3 (ou optionnellent v1.2/1.2 ou v1/1)
		// on accepte eventuellement un 4e digit
		if (preg_match(",^v?(\d+)(\.(\d+))?(\.(\d+))?(\.(\d+))?(-(alpha|beta|dev|rc).*)?$,i", $tag_name)) {
			// gerer les malvenus / dans les noms de tags : on remplace par un -
			if (strpos($tag_name, '/') !== false) {
				$tag_name_clean = str_replace("/", "-", $tag_name);
				$zip_file = str_replace($tag_name, $tag_name_clean, $tag_detail['zipball_url']);
				$zip_file = basename($zip_file);
			}
			else {
				$zip_file = basename($tag_detail['zipball_url']);
			}
			$zip_file = $dir_source . $zip_file;

			$new = false;
			if (!file_exists($zip_file) or !filesize($zip_file)) {
				$new = true;
				// on curl le zip
				passthru("curl --silent -L ".escapeshellarg($tag_detail['zipball_url'])." > $zip_file");
				if (!file_exists($zip_file)) {
					return "Echec recuperation zip ".$tag_detail['zipball_url'];
				}
				if ($timestamp = gitea_get_commit_timestamp($endpoint, $repository, $tag_detail['sha'])) {
					// mettre le zip a la date du tag
					touch($zip_file, $timestamp);
				}
			}

			$tags[$tag_name] = [
				'new' => $new,
				'zip' => $zip_file
			];
		}
	}

	debardeur_nettoyer_source($dir_source, $tags);

	return $tags;
}

/**
 * Lister les repositories d'une organisation
 * @param string $url_organisation
 * @param int $last_modified_time
 * @return array
 */
function debardeur_connecteur_gitea_lister_repositories($url_organisation, $last_modified_time = null) {
	$endpoint = gitea_endpoint_from_url($url_organisation);
	$org = gitea_organisation_from_url($url_organisation);

	$method = "orgs/{$org}/repos";
	$res = debardeur_json_api_call_pages(0, 'gitea', $endpoint, $method, ['limit' => 50], $last_modified_time);

	$repositories = [];
	if ($res) {
		foreach ($res as $k => $row) {
			if ($k === 'message' and is_string($row)) {
				debardeur_fail("Echec API Gitea", implode("\n", [$url_organisation, "API Call $endpoint $method", $row, json_encode($res)]));
			}
			if (is_array($row)
				and !empty($row['clone_url'])
				and empty($row['empty'])) {
				$repositories[$row['clone_url']] = [
					'name' => $row['name'],
					'full_name' => $row['full_name'],
					'url' => $row['clone_url'],
					'last_modified' => strtotime($row['updated_at'])
				];
			}
		}
	}

	return $repositories;
}

/****************************************/


function gitea_endpoint_from_url($url){
	$url = explode('://', $url, 2);
	$host = explode("/", end($url), 2);
	$endpoint = $url[0] . "://" . $host[0] . "/api/v1/";

	return $endpoint;
}


function gitea_repository_from_url($url){
	$url = explode('://', $url, 2);
	$path = explode("/", end($url), 2);
	$path = end($path);
	$repository = preg_replace(",\.git$,", "", $path);

	return $repository;
}


function gitea_organisation_from_url($url){
	$repository = gitea_repository_from_url($url);
	$repository = explode('/', $repository);
	$organisation = reset($repository);

	return $organisation;
}

/**
 * Recuperer les tags d'un projet gitea
 * @param string $endpoint
 * @param string $repository
 * @return bool|array
 */
function gitea_get_tags($endpoint, $repository, $last_modified=null) {

	$method = "repos/" . $repository . "/tags";
	$liste = debardeur_json_api_call_pages(0, 'gitea', $endpoint, $method, ['limit' => 50], $last_modified);
	if ($liste === false or !is_array($liste)) {
		if ($liste) {
			spip_log("gitea_get_tags: retour innatendu " . json_encode($liste), "debardeur");
		}
		return false;
	}

	/*
	[
	  {
	    "name": "V0.4.9",
	    "id": "0cca7f5c9c716028b93eb62dd62e49fcc0c22444",
	    "commit": {
	      "url": "https://git.spip.net/api/v1/repos/spip-contrib-extensions/commandes/git/commits/0cca7f5c9c716028b93eb62dd62e49fcc0c22444",
	      "sha": "0cca7f5c9c716028b93eb62dd62e49fcc0c22444"
	    },
	    "zipball_url": "https://git.spip.net/spip-contrib-extensions/commandes/archive/V0.4.9.zip",
	    "tarball_url": "https://git.spip.net/spip-contrib-extensions/commandes/archive/V0.4.9.tar.gz"
	  }
	]
	*/

	$tags = [];
	foreach($liste as $t) {
		if (!isset($t['zipball_url'])) {
			return false;
		}
		$tag = [
			'sha' => $t['commit']['sha'],
			'zipball_url' => $t['zipball_url'],
			'tarball_url' => $t['tarball_url'],
		];
		$tags[$t['name']] = $tag;
	}

	return $tags;
}

/**
 * Retrouve le timestamp d'un commit
 * @param $endpoint
 * @param $repository
 * @param $sha
 * @return bool|int
 */
function gitea_get_commit_timestamp($endpoint, $repository, $sha) {
	$method = "repos/" . $repository . "/git/commits/$sha";
	$infos = debardeur_json_api_call('gitea', $endpoint, $method);
	if (!$infos) {
		return false;
	}

	// attention, ici on est pas identique a github : il y a un sous tableau 'commit'
	$date = '';
	if (isset($infos['commit']['committer']['date'])) {
		$date = $infos['commit']['committer']['date'];
	}
	elseif (isset($infos['commit']['author']['date'])) {
		$date = $infos['commit']['author']['date'];
	}
	return $date ? strtotime($date) : 0;
}
