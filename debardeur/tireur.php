<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param array $liste_sources
 * @param bool $force_update
 * @param string|null $dir_sources
 * @return bool
 * @throws Exception
 */
function debardeur_tirer($liste_sources, $force_update = false, $dir_sources=null) {
	include_spip('inc/debardeur');
	debardeur_init();

	if (is_null($dir_sources)) {
		$dir_sources = _DIR_DEBARDEUR_SOURCES;
	}
	debardeur_check_dir($dir_sources);

	foreach ($liste_sources as $source){
		$connecteur = debardeur_trouver_connecteur($source['methode'], $source['url']);
		debardeur_log("<info>--- Source " . $source['short'] . " | " . $source['slug'] . " | " . $connecteur . " | " . $source['url']."</info>");

		$dir_source = $dir_sources . $source['dir_checkout'] . '/';

		$recuperer_tags = debardeur_connecteur_function($connecteur, 'recuperer_tags');
		$tags = $recuperer_tags($source['url'], $dir_source, $force_update);

		if ($tags === false or is_string($tags)) {
			$corps = $source['url'] . ' | ' . $source['dir_checkout'] . "\n" . "Erreur lors du checkout";
			if (is_string($tags)) {
				$corps .= "\n$tags";
			}
			debardeur_fail('[Tireur] : Erreur', $corps);
		}

		$new_tags = [];
		$existing_tags = [];
		foreach ($tags as $tag_name => $infos) {
			if ($infos['new']){
				$new_tags[$tag_name] = $infos;
			} else {
				$existing_tags[$tag_name] = $infos;
			}
		}

		$nb_existing = count($existing_tags);
		if ($nb_existing) {
			debardeur_log("  |- $nb_existing Tags déjà existants dans $dir_source : " . implode(', ', array_keys($existing_tags)));
		}
		foreach ($new_tags as $tag_name => $infos) {
			debardeur_log("  |- Nouveau Tag $tag_name | " . $infos['zip']);
		}

	}

	return true;
}

