# debardeur en local

Installer spip, spip-cli, checkout et spip-archives

À la racine du site spip :

```bash
mkdir -p plugins
cd plugins
git clone git@git.spip.net:spip-contrib-extensions/debardeur.git
cd -
mkdir -p debardeur/{archivelists,sources,depots,tmp}
echo "git;git@git.spip.net:spip/spip.git" > debardeur/archivelists/archivelist.txt
```
