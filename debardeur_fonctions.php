<?php
/**
 * Fonctions utiles au plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
