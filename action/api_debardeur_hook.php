<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Actions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


function action_api_debardeur_hook_dist($arg = null) {

	if (is_null($arg)) {
		$arg = _request('arg');
	}

	// on attend une url du type gitlab/update
	// si pas de source en premier, c'est gitea (historique)
	$arg = explode('/', $arg);
	$source = array_shift($arg);
	if (!count($arg) || (!$action = reset($arg))) {
		$action = $source;
		$source = 'gitea';
	}

	$payload = file_get_contents('php://input');
	spip_log("api_debardeur_hook: $action $payload", 'debardeur');

	if (!$payload || (!$data = json_decode($payload, true))) {
		spip_log('api_debardeur_hook: payload format json incorrect', 'debardeur' . _LOG_ERREUR);
		http_response_code(403);
		exit;
	}

	if (function_exists($f = "action_api_debardeur_hook_" . $source . '_' . $action)
		|| function_exists($f = "action_api_debardeur_hook_" . $source . '_' . $action . '_dist')) {
		$f($data);
	}

	include_spip('inc/headers');
	http_response_code(204); // No Content
	header("Connection: close");
	exit;
}

/*
 Notifications depuis Gitea
{
"secret": "",
"ref": "refs/heads/master",
"before": "0000000000000000000000000000000000000000",
"after": "78d2f605c75a520140d4200c22767680d441d3d4",
"compare_url": "https://git.spip.net/",
"commits": [
  {
    "id": "78d2f605c75a520140d4200c22767680d441d3d4",
    "message": "Simplification du code du plugin:\n- une seule règle pour tous les cas\n- la callback construit les titres y compris pour la numérotation\n- le pipeline pre_propre n'est plus utile.\nUn peu de P$
    "url": "https://git.spip.net/spip-contrib-extensions/porte_plume_intertitres/commit/78d2f605c75a520140d4200c22767680d441d3d4",
    "author": {
      "name": "Eric Lupinacci",
      "email": "eric@smellup.net",
      "username": "Eric"
    },
    "committer": {
      "name": "Eric Lupinacci",
      "email": "eric@smellup.net",
      "username": "Eric"
    },
    "verification": null,
    "timestamp": "2020-04-12T18:34:54+02:00",
    "added": [],
    "removed": [],
    "modified": [
      "paquet.xml",
      "porte_plume_intertitres_options.php",
      "porte_plume_intertitres_pipelines.php",
      "wheels/porte_plume_intertitres.php",
      "wheels/porte_plume_intertitres.yaml"
    ]
  },
...
],
"head_commit": null,
"repository": {
  "id": 2002,
  "owner": {
    "id": 6928,
    "login": "spip-contrib-extensions",
    "full_name": "Extensions fonctionnelles de SPIP",
    "email": "",
    "avatar_url": "https://git.spip.net/user/avatar/spip-contrib-extensions/-1",
    "language": "",
    "is_admin": false,
    "last_login": "1970-01-01T01:00:00+01:00",
    "created": "2020-01-18T18:23:58+01:00",
    "username": "spip-contrib-extensions"
  },
  "name": "porte_plume_intertitres",
  "full_name": "spip-contrib-extensions/porte_plume_intertitres",
  "description": "",
  "empty": false,
  "private": false,
  "fork": false,
  "template": false,
  "parent": null,
  "mirror": false,
  "size": 75,
  "html_url": "https://git.spip.net/spip-contrib-extensions/porte_plume_intertitres",
  "ssh_url": "git@git.spip.net:spip-contrib-extensions/porte_plume_intertitres.git",
  "clone_url": "https://git.spip.net/spip-contrib-extensions/porte_plume_intertitres.git",
  "original_url": "",
  "website": "",
  "stars_count": 0,
  "forks_count": 0,
  "watchers_count": 478,
  "open_issues_count": 0,
  "open_pr_counter": 0,
  "release_counter": 0,
  "default_branch": "master",
  "archived": false,
  "created_at": "2020-04-12T19:18:53+02:00",
  "updated_at": "2020-04-12T19:23:35+02:00",
  "permissions": {
    "admin": true,
    "push": true,
    "pull": true
  },
  "has_issues": true,
	"internal_tracker": {
	  "enable_time_tracker": true,
	  "allow_only_contributors_to_track_time": true,
	  "enable_issue_dependencies": true
	},
	"has_wiki": true,
	"has_pull_requests": true,
	"ignore_whitespace_conflicts": false,
	"allow_merge_commits": true,
	"allow_rebase": true,
	"allow_rebase_explicit": true,
	"allow_squash_merge": true,
	"avatar_url": ""
},
"pusher": {
	"id": 167,
	"login": "Eric",
	"full_name": "Eric Lupinacci",
	"email": "eric@noreply.git.spip.net",
	"avatar_url": "https://git.spip.net/user/avatar/Eric/-1",
	"language": "fr-FR",
	"is_admin": true,
	"last_login": "2020-03-22T15:32:00+01:00",
	"created": "2017-05-26T10:41:58+02:00",
	"username": "Eric"
},
"sender": {
  "id": 167,
  "login": "Eric",
  "full_name": "Eric Lupinacci",
  "email": "eric@noreply.git.spip.net",
  "avatar_url": "https://git.spip.net/user/avatar/Eric/-1",
  "language": "fr-FR",
  "is_admin": true,
  "last_login": "2020-03-22T15:32:00+01:00",
  "created": "2017-05-26T10:41:58+02:00",
  "username": "Eric"
}
}

Pour un tag :
{
  "secret": "",
  "ref": "refs/tags/1.1.0",
  "before": "0000000000000000000000000000000000000000",
  "after": "204faf0b23310a7221fabea96beb030169a58c47",
  "compare_url": "https://git.spip.net/",
  "commits": [],
  "head_commit": null,
...
}
 *
 *
 *
 */

/**
 * @param $data
 */
function action_api_debardeur_hook_gitea_update_dist($data) {

	// est-ce bien gitea qui nous notifie ?
	if (!empty($data['repository']['clone_url']) and strpos($data['repository']['clone_url'], 'https://git.spip.net/') === 0) {
		spip_log('action_api_debardeur_hook_gitea_update_dist : ' . $data['repository']['clone_url'], 'debardeur' . _LOG_DEBUG);

		include_spip('inc/debardeur');
		debardeur_init();

		// seulement si c'est une pose de tag
		if (!empty($data['ref']) and strpos($data['ref'], 'refs/tags/') !== false) {

			$url = $data['repository']['clone_url'];
			$infos = [
				'methode' => 'git',
				'url' => $url,
				'short' => $data['repository']['name']
			];
			$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'updated-tags');
			$json_file = $dir . md5($url) . '.json';
			file_put_contents($json_file, json_encode($infos));
			spip_log("api_debardeur_hook_update: fichier $json_file ajoute", 'debardeur');
		}

		// si il y a des commits, on note dans un dossier pour salvatore qui doit recharger les plugins concernes
		if (!empty($data['commits'])) {
			$url = $data['repository']['clone_url'];

			$branch = '';
			if (isset($data['ref_type']) and $data['ref_type'] === 'branch') {
				$branch = $data['ref'];
			} elseif (isset($data['ref']) and strpos($data['ref'], 'refs/heads/') === 0) {
				// sur v2
				$refs = explode('/', $data['ref']);
				$branch = end($refs);
			}

			$infos = [
				'methode' => 'git',
				'url' => $url,
				'branche' => $branch,
				'short' => $data['repository']['name'],
				'time' => time(),
			];

			$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'updated-files');
			$json_file = $dir . md5("$url|$branch") . '.json';
			file_put_contents($json_file, json_encode($infos));
			spip_log("api_debardeur_hook_update: fichier $json_file ajoute", 'debardeur');
		}

		$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'pushed');
		debardeur_forward_event($dir, $data, $data['repository']['clone_url']);
	}
}


function action_api_debardeur_hook_gitlab_test_dist($data) {

	// TODO
	spip_log("action_api_debardeur_hook_gitlab_test_dist: " . json_encode($data), 'gitlabdbg' . _LOG_DEBUG);

}

function action_api_debardeur_hook_gitlab_project_update_dist($data) {

	spip_log("action_api_debardeur_hook_gitlab_project_update_dist: " . json_encode($data), 'gitlabdbg' . _LOG_DEBUG);

	// on n'accepte que les notifs de git.spip.net
	if (
		!empty($data['project']['git_http_url'])
		&& ($clone_url = $data['project']['git_http_url'])
		&& strpos($clone_url, 'https://git.spip.net/') === 0
	) {

		include_spip('inc/debardeur');
		debardeur_init();

		if (!empty($data['object_kind'])) {
			$forward_event = false;
			switch ($data['object_kind']) {
				case 'note':
					/*
					 2024-03-02 19:06:06 51.91.31.20 (pid 1078821) :Pub:info: api_debardeur_hook: project_update {"object_kind":"note","event_type":"note","user":{"id":9,"name":"cerdic","username":"cerdic","avatar_url":"https://secure.gravatar.com/avatar/f651a763484252616f6a68de1cedbf25?s=80&d=identicon","email":"cedric@yterium.com"},"project_id":1916,"project":{"id":1916,"name":"debardeur","description":"","web_url":"https://git.spip.net/spip-contrib-extensions/debardeur","avatar_url":"https://git.spip.net/uploads/-/system/project/avatar/1916/_spip-contrib-extensions_debardeur.png","git_ssh_url":"git@git.spip.net:spip-contrib-extensions/debardeur.git","git_http_url":"https://git.spip.net/spip-contrib-extensions/debardeur.git","namespace":"spip-contrib-extensions","visibility_level":20,"path_with_namespace":"spip-contrib-extensions/debardeur","default_branch":"master","ci_config_path":null,"homepage":"https://git.spip.net/spip-contrib-extensions/debardeur","url":"git@git.spip.net:spip-contrib-extensions/debardeur.git","ssh_url":"git@git.spip.net:spip-contrib-extensions/debardeur.git","http_url":"https://git.spip.net/spip-contrib-extensions/debardeur.git"},"object_attributes":{"attachment":null,"author_id":9,"change_position":null,"commit_id":null,"created_at":"2024-03-02 19:06:05 +0100","discussion_id":"7e6986013a57309bf3895d451862226ae59f5a8b","id":176579,"line_code":null,"note":"ça semble bon sur les commits et les push de branche, a voir pour les tags, les issues, PR et commentaires","noteable_id":28658,"noteable_type":"Issue","original_position":null,"position":null,"project_id":1916,"resolved_at":null,"resolved_by_id":null,"resolved_by_push":null,"st_diff":null,"system":false,"type":null,"updated_at":"2024-03-02 19:06:05 +0100","updated_by_id":null,"description":"ça semble bon sur les commits et les push de branche, a voir pour les tags, les issues, PR et commentaires","url":"https://git.spip.net/spip-contrib-extensions/debardeur/-/issues/15#note_176579"},"repository":{"name":"debardeur","url":"git@git.spip.net:spip-contrib-extensions/debardeur.git","description":"","homepage":"https://git.spip.net/spip-contrib-extensions/debardeur"},"issue":{"author_id":9,"closed_at":null,"confidential":false,"created_at":"2024-02-27 17:28:02 +0100","description":"22eb6e21a823feb7d55e3014b8a0682e8fffab8f a ajouté un niveau d'aiguillage pour supporter des events de plusieurs sources possibles. Par défaut c'est gitea (si pas précisé) historique. Et on peut donc supporter gitlab maintenant.","discussion_locked":null,"due_date":null,"id":28658,"iid":15,"last_edited_at":null,"last_edited_by_id":null,"milestone_id":null,"moved_to_id":null,"duplicated_to_id":null,"project_id":1916,"relative_position":null,"state_id":1,"time_estimate":0,"title":"Support des webhooks gitlab","updated_at":"2024-03-02 19:06:05 +0100","updated_by_id":null,"url":"https://git.spip.net/spip-contrib-extensions/debardeur/-/issues/15","total_time_spent":0,"time_change":0,"human_total_time_spent":null,"human_time_change":null,"human_time_estimate":null,"assignee_ids":[],"assignee_id":null,"labels":[],"state":"opened","severity":"unknown","customer_relations_contacts":[]}}
					 */
					$forward_event = true;
					break;

				case 'issue':
				case 'merge_request':
					$forward_event = true;
					break;
			}

			if ($forward_event) {
				// on ajoute le contenu de la notif dans un dossier notif pour envoyer par mail a spip-zone-commit
				$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'pushed');
				debardeur_forward_event($dir, $data, $clone_url);
			}
		}
	}

}

function action_api_debardeur_hook_gitlab_general_update_dist($data) {

	spip_log("action_api_debardeur_hook_gitlab_general_update_dist: " . json_encode($data), 'gitlabdbg' . _LOG_DEBUG);

	// renseigner a minima le projet lors de la creation
	if (isset($data['event_name']) and $data['event_name'] === 'project_create' and empty($data['project'])) {
		$data['project'] = [
			'id' => $data['project_id'],
			'git_http_url' => 'https://git.spip.net/' . $data['path_with_namespace'] . '.git'
		];
	}

	// on n'accepte que les notifs de git.spip.net
	if (!empty($data['project']['git_http_url'])
		and $clone_url = $data['project']['git_http_url']
		and strpos($clone_url, 'https://git.spip.net/') === 0) {

		include_spip('inc/debardeur');
		debardeur_init();

		if (!empty($data['event_name'])) {
			$forward_event = false;
			switch ($data['event_name']) {
				case 'project_create':
					/*
					 2024-03-06 11:58:37 51.91.31.20 (pid 3616494) :Pub:debug: action_api_debardeur_hook_gitlab_general_update_dist: {"event_name":"project_create","created_at":"2024-03-06T11:58:37+01:00","updated_at":"2024-03-06T11:58:37+01:00","name":"image_aplatit_png","path":"image_aplatit_png","path_with_namespace":"spip-contrib-extensions\/image_aplatit_png","project_id":3230,"owner_name":"spip-contrib-extensions","owner_email":"","owners":[{"name":"Administrator","email":"[REDACTED]"}],"project_visibility":"public"}
					 */
					$project = $data['project'];
					$forward_event = true;
					break;

				case 'repository_update':
					/*
					 2024-03-02 15:10:08 51.91.31.20 (pid 1078796) :Pub:debug: action_api_debardeur_hook_gitlab_test_dist: {"event_name":"repository_update","user_id":27,"user_name":"salvatore","user_email":"salvatore@rezo.net","user_avatar":"https:\/\/secure.gravatar.com\/avatar\/3838989165065d71d723d69e10753b0a?s=80&d=identicon","project_id":1703,"project":{"id":1703,"name":"urls_etendues","description":"","web_url":"https:\/\/git.spip.net\/spip\/urls_etendues","avatar_url":"https:\/\/git.spip.net\/uploads\/-\/system\/project\/avatar\/1703\/_spip_urls_etendues.png","git_ssh_url":"git@git.spip.net:spip\/urls_etendues.git","git_http_url":"https:\/\/git.spip.net\/spip\/urls_etendues.git","namespace":"spip","visibility_level":20,"path_with_namespace":"spip\/urls_etendues","default_branch":"master","ci_config_path":null,"homepage":"https:\/\/git.spip.net\/spip\/urls_etendues","url":"git@git.spip.net:spip\/urls_etendues.git","ssh_url":"git@git.spip.net:spip\/urls_etendues.git","http_url":"https:\/\/git.spip.net\/spip\/urls_etendues.git"},"changes":[{"before":"aa5d70486d5bbeade74602d96f9816f90b900edd","after":"46805d6ac9894857da925f5d9b534916a77cd333","ref":"refs\/heads\/master"}],"refs":["refs\/heads\/master"]}
					 */
					$project = $data['project'];
					// il faut aller chercher les infos du projet via api car on a pas grand chose dans cet event, on passe
					break;

				case 'tag_push':
					/*
					 Ajout d'un tag
					 2024-03-04 11:42:07 51.91.31.20 (pid 2299746) :Pub:debug: action_api_debardeur_hook_gitlab_project_update_dist: {"object_kind":"tag_push","event_name":"tag_push","before":"0000000000000000000000000000000000000000","after":"a80744003f15c0f26eccdf5e2092ff217519f69e","ref":"refs\/tags\/v3.0.5","ref_protected":false,"checkout_sha":"a80744003f15c0f26eccdf5e2092ff217519f69e","message":"","user_id":9,"user_name":"cerdic","user_username":"cerdic","user_email":"cedric@yterium.com","user_avatar":"https:\/\/secure.gravatar.com\/avatar\/f651a763484252616f6a68de1cedbf25?s=80&d=identicon","project_id":2121,"project":{"id":2121,"name":"indexer","description":"","web_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer","avatar_url":"https:\/\/git.spip.net\/uploads\/-\/system\/project\/avatar\/2121\/_spip-contrib-extensions_indexer.png","git_ssh_url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","git_http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer.git","namespace":"spip-contrib-extensions","visibility_level":20,"path_with_namespace":"spip-contrib-extensions\/indexer","default_branch":"master","ci_config_path":null,"homepage":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer","url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","ssh_url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer.git"},"commits":[{"id":"a80744003f15c0f26eccdf5e2092ff217519f69e","message":"fix: si on modifie la timezone pour lire la date du document, la remettre a sa valeur initiale ensuite pour ne pas perturber la suite du script\n","title":"fix: si on modifie la timezone pour lire la date du document, la remettre a sa...","timestamp":"2024-03-04T11:41:05+01:00","url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer\/-\/commit\/a80744003f15c0f26eccdf5e2092ff217519f69e","author":{"name":"Cerdic","email":"cedric@yterium.com"},"added":[],"modified":["lib\/Indexer\/Storage\/Sphinx.php","paquet.xml"],"removed":[]}],"total_commits_count":1,"push_options":[],"repository":{"name":"indexer","url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","description":"","homepage":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer","git_http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer.git","git_ssh_url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","visibility_level":20}}
					Suppression d'un tag
					2024-03-04 11:49:15 51.91.31.20 (pid 2299717) :Pub:debug: action_api_debardeur_hook_gitlab_general_update_dist: {"object_kind":"tag_push","event_name":"tag_push","before":"a80744003f15c0f26eccdf5e2092ff217519f69e","after":"0000000000000000000000000000000000000000","ref":"refs\/tags\/v3.0.5","ref_protected":false,"checkout_sha":null,"message":null,"user_id":9,"user_name":"cerdic","user_username":"cerdic","user_email":"cedric@yterium.com","user_avatar":"https:\/\/secure.gravatar.com\/avatar\/f651a763484252616f6a68de1cedbf25?s=80&d=identicon","project_id":2121,"project":{"id":2121,"name":"indexer","description":"","web_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer","avatar_url":"https:\/\/git.spip.net\/uploads\/-\/system\/project\/avatar\/2121\/_spip-contrib-extensions_indexer.png","git_ssh_url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","git_http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer.git","namespace":"spip-contrib-extensions","visibility_level":20,"path_with_namespace":"spip-contrib-extensions\/indexer","default_branch":"master","ci_config_path":null,"homepage":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer","url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","ssh_url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer.git"},"commits":[],"total_commits_count":0,"push_options":[],"repository":{"name":"indexer","url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","description":"","homepage":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer","git_http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/indexer.git","git_ssh_url":"git@git.spip.net:spip-contrib-extensions\/indexer.git","visibility_level":20}}
					 */
					// seulement si c'est une pose de tag
					$project = $data['project'];
					if (!empty($data['ref']) and strpos($data['ref'], 'refs/tags/') !== false) {

						$infos = [
							'methode' => 'git',
							'url' => $clone_url,
							'short' => $project['name']
						];
						$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'updated-tags');
						$json_file = $dir . md5($clone_url) . '.json';
						file_put_contents($json_file, json_encode($infos));
						spip_log("action_api_debardeur_hook_gitlab_general_update_dist: fichier $json_file ajoute", 'debardeur');
					}
					// et on break pas : on continue sur le cas general 'push'

				case 'push':
					/*
					 Push commit
					 2024-03-02 15:10:07 51.91.31.20 (pid 1078799) :Pub:debug: action_api_debardeur_hook_gitlab_test_dist: {"object_kind":"push","event_name":"push","before":"aa5d70486d5bbeade74602d96f9816f90b900edd","after":"46805d6ac9894857da925f5d9b534916a77cd333","ref":"refs\/heads\/master","ref_protected":true,"checkout_sha":"46805d6ac9894857da925f5d9b534916a77cd333","message":null,"user_id":27,"user_name":"salvatore","user_username":"salvatore","user_email":null,"user_avatar":"https:\/\/secure.gravatar.com\/avatar\/3838989165065d71d723d69e10753b0a?s=80&d=identicon","project_id":1703,"project":{"id":1703,"name":"urls_etendues","description":"","web_url":"https:\/\/git.spip.net\/spip\/urls_etendues","avatar_url":"https:\/\/git.spip.net\/uploads\/-\/system\/project\/avatar\/1703\/_spip_urls_etendues.png","git_ssh_url":"git@git.spip.net:spip\/urls_etendues.git","git_http_url":"https:\/\/git.spip.net\/spip\/urls_etendues.git","namespace":"spip","visibility_level":20,"path_with_namespace":"spip\/urls_etendues","default_branch":"master","ci_config_path":null,"homepage":"https:\/\/git.spip.net\/spip\/urls_etendues","url":"git@git.spip.net:spip\/urls_etendues.git","ssh_url":"git@git.spip.net:spip\/urls_etendues.git","http_url":"https:\/\/git.spip.net\/spip\/urls_etendues.git"},"commits":[{"id":"46805d6ac9894857da925f5d9b534916a77cd333","message":"i18n: [Salvatore] [source:lang\/ paquet-urls] Export depuis https:\/\/trad.spip.net de la langue eu\ni18n: [Salvatore] [source:lang\/ paquet-urls] Mise a jour du bilan depuis https:\/\/trad.spip.net\n","title":"i18n: [Salvatore] [source:lang\/ paquet-urls] Export depuis https:\/\/trad.spip.net de la langue eu","timestamp":"2024-03-02T15:10:05+01:00","url":"https:\/\/git.spip.net\/spip\/urls_etendues\/-\/commit\/46805d6ac9894857da925f5d9b534916a77cd333","author":{"name":"otalazt","email":"otalazt@gmail.com"},"added":["lang\/paquet-urls_eu.php"],"modified":["lang\/paquet-urls.xml"],"removed":[]}],"total_commits_count":1,"push_options":[],"repository":{"name":"urls_etendues","url":"git@git.spip.net:spip\/urls_etendues.git","description":"","homepage":"https:\/\/git.spip.net\/spip\/urls_etendues","git_http_url":"https:\/\/git.spip.net\/spip\/urls_etendues.git","git_ssh_url":"git@git.spip.net:spip\/urls_etendues.git","visibility_level":20}}
					 Push Branch
					 {"object_kind":"push","event_name":"push","before":"0000000000000000000000000000000000000000","after":"78134ed10a214db4ad29bb3a32127d89b43933ed","ref":"refs\/heads\/test_notification","ref_protected":false,"checkout_sha":"78134ed10a214db4ad29bb3a32127d89b43933ed","message":null,"user_id":9,"user_name":"cerdic","user_username":"cerdic","user_email":"cedric@yterium.com","user_avatar":"https:\/\/secure.gravatar.com\/avatar\/f651a763484252616f6a68de1cedbf25?s=80&d=identicon","project_id":1916,"project":{"id":1916,"name":"debardeur","description":"","web_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/debardeur","avatar_url":"https:\/\/git.spip.net\/uploads\/-\/system\/project\/avatar\/1916\/_spip-contrib-extensions_debardeur.png","git_ssh_url":"git@git.spip.net:spip-contrib-extensions\/debardeur.git","git_http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/debardeur.git","namespace":"spip-contrib-extensions","visibility_level":20,"path_with_namespace":"spip-contrib-extensions\/debardeur","default_branch":"master","ci_config_path":null,"homepage":"https:\/\/git.spip.net\/spip-contrib-extensions\/debardeur","url":"git@git.spip.net:spip-contrib-extensions\/debardeur.git","ssh_url":"git@git.spip.net:spip-contrib-extensions\/debardeur.git","http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/debardeur.git"},"commits":[],"total_commits_count":0,"push_options":[],"repository":{"name":"debardeur","url":"git@git.spip.net:spip-contrib-extensions\/debardeur.git","description":"","homepage":"https:\/\/git.spip.net\/spip-contrib-extensions\/debardeur","git_http_url":"https:\/\/git.spip.net\/spip-contrib-extensions\/debardeur.git","git_ssh_url":"git@git.spip.net:spip-contrib-extensions\/debardeur.git","visibility_level":20}}
					 Delete Branch
					 2024-03-02 17:50:14 51.91.31.20 (pid 1078827) :Pub:info: api_debardeur_hook: general_update {"event_name":"repository_update","user_id":9,"user_name":"cerdic","user_email":"cedric@yterium.com","user_avatar":"https://secure.gravatar.com/avatar/f651a763484252616f6a68de1cedbf25?s=80&d=identicon","project_id":1916,"project":{"id":1916,"name":"debardeur","description":"","web_url":"https://git.spip.net/spip-contrib-extensions/debardeur","avatar_url":"https://git.spip.net/uploads/-/system/project/avatar/1916/_spip-contrib-extensions_debardeur.png","git_ssh_url":"git@git.spip.net:spip-contrib-extensions/debardeur.git","git_http_url":"https://git.spip.net/spip-contrib-extensions/debardeur.git","namespace":"spip-contrib-extensions","visibility_level":20,"path_with_namespace":"spip-contrib-extensions/debardeur","default_branch":"master","ci_config_path":null,"homepage":"https://git.spip.net/spip-contrib-extensions/debardeur","url":"git@git.spip.net:spip-contrib-extensions/debardeur.git","ssh_url":"git@git.spip.net:spip-contrib-extensions/debardeur.git","http_url":"https://git.spip.net/spip-contrib-extensions/debardeur.git"},"changes":[{"before":"78134ed10a214db4ad29bb3a32127d89b43933ed","after":"0000000000000000000000000000000000000000","ref":"refs/heads/test_notification"}],"refs":["refs/heads/test_notification"]}
					 */
					$project = $data['project'];
					$forward_event = true;
					// si il y a des commits, on note dans un dossier pour salvatore qui doit recharger les plugins concernes
					if (!empty($data['commits'])) {
						$url = $clone_url;

						$branch = '';
						if (isset($data['ref']) and strpos($data['ref'], 'refs/heads/') === 0) {
							// sur v2
							$refs = explode('/', $data['ref'], 3);
							$branch = end($refs);
						}

						$infos = [
							'methode' => 'git',
							'url' => $clone_url,
							'branche' => $branch,
							'short' => $project['name'],
							'time' => time(),
						];

						$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'updated-files');
						$json_file = $dir . md5("$url|$branch") . '.json';
						file_put_contents($json_file, json_encode($infos));
						spip_log("api_debardeur_hook_update: fichier $json_file ajoute", 'debardeur' . _LOG_DEBUG);
					}
					// verifier que le projet a bien un webhook perso pour avoir plus d'info !
					// en async
					if (defined('_GITLAB_API_TOKEN')) {
						$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'webhook-checked');
						if (!file_exists($f = $dir . $data['project_id']. '.ok.json')) {
							job_queue_add('debardeur_check_project_webhook', 'Check Webhook project '. $project['name'], [$project, $f], 'action/api_debardeur_hook');
						}
					}

					break;
			}

			if ($forward_event) {
				// on ajoute le contenu de la notif dans un dossier notif pour envoyer par mail a spip-zone-commit
				$dir = sous_repertoire(_DIR_DEBARDEUR_TMP, 'pushed');
				debardeur_forward_event($dir, $data, $clone_url);
			}
		}
	}

}


function debardeur_check_project_webhook($project, $file_token) {
	if (!defined('_GITLAB_API_TOKEN')) {
		return;
	}
	include_spip('inc/debardeur');
	include_spip('debardeur/connecteur/gitlab');
	$endpoint = gitlab_endpoint_from_url($project['web_url']);
	$project_id = $project['id'];
	$options = [
		'headers' => [
			"PRIVATE-TOKEN" => _GITLAB_API_TOKEN,
		]
	];
	$data_hook = [
		'url' => 'https://contrib.spip.net/debardeur_hook.api/gitlab/project_update/',
		'issues_events' => true,
		'merge_requests_events' => true,
		'note_events' => true,
		'tag_push_events' => true,
		'wiki_page_events' => true,
	];

	$url_api = $endpoint ."projects/{$project_id}/hooks";
	$existing_hooks = debardeur_json_recuperer_url($url_api, $options);
	if ($existing_hooks) {
		spip_log("debardeur_check_project_webhook: project #$project_id : hooks " .json_encode($existing_hooks), 'gitlabdbg' . _LOG_DEBUG);
		// retrouver le hook qu'on a deja mis avant et voir si il est bon
		//[{"id":5,"url":"https:\/\/contrib.spip.net\/debardeur_hook.api\/gitlab\/project_update\/","created_at":"2024-03-02T18:45:46.954+01:00","push_events":true,"tag_push_events":true,"merge_requests_events":false,"repository_update_events":false,"enable_ssl_verification":true,"alert_status":"executable","disabled_until":null,"project_id":1916,"issues_events":true,"confidential_issues_events":false,"note_events":true,"confidential_note_events":null,"pipeline_events":false,"wiki_page_events":true,"deployment_events":false,"job_events":false,"releases_events":false,"push_events_branch_filter":null,"emoji_events":false}]
		foreach ($existing_hooks as $hook) {
			if ($hook['url'] === $data_hook['url']) {
				foreach ($data_hook as $k => $v) {
					if ($hook[$k] !== $v) {
						// une valeur est pas bonne, il faut le modifier
						$data_hook['id'] = $hook['id'];
						break 2;
					}
				}
				// ici ça veut dire que le hook est deja comme on veut, rien à faire
				spip_log("debardeur_check_project_webhook: project #$project_id hook #" .$hook['id'] .' OK, RAS', 'debardeur' . _LOG_DEBUG);
				@file_put_contents($file_token, json_encode($project));
				return;
			}
		}
	}
	if (isset($data_hook['id'])) {
		$options['methode'] = 'PUT';
		$url_api .= '/' . $data_hook['id'];
		spip_log("debardeur_check_project_webhook: project #$project_id hook #" .$data_hook['id'] .' incomplet, on le modifie', 'debardeur' . _LOG_DEBUG);
	} else {
		$options['methode'] = 'POST';
		spip_log("debardeur_check_project_webhook: project #$project_id ajout d'un nouveau hook", 'debardeur' . _LOG_DEBUG);
	}
	$options['datas'] = $data_hook;
	$res = debardeur_json_recuperer_url($url_api, $options);
	if ($res && isset($res['url']) && $res['url'] === $data_hook['url']) {
		spip_log("debardeur_check_project_webhook: project #$project_id : POST hook OK " .json_encode($res), 'debardeur' . _LOG_DEBUG);
		@file_put_contents($file_token, json_encode($project));
	}
	else {
		spip_log("debardeur_check_project_webhook: project #$project_id : POST hook Echec " .json_encode($res), 'debardeur' . _LOG_ERREUR);
	}
}

function debardeur_forward_event($dir, $data, $clone_url) {
	// on ajoute le contenu de la notif dans un dossier notif pour envoyer par mail a spip-zone-commit
	$json = json_encode($data);
	$hash = substr(md5($json), 0, 8);
	$json_file = $dir . preg_replace(",\W+,", '-', $clone_url) . "--{$hash}.json";
	file_put_contents($json_file, $json);
	spip_log("action_api_debardeur_hook:debardeur_forward_event: fichier $json_file ajoute", 'debardeur' . _LOG_DEBUG);
}
