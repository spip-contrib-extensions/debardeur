<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Inc
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip("inc/empaqueteur_paquet");
include_spip("inc/empaqueteur_plugin");

/**
 * Analyser un zip pas encore traite
 * on le dezip et on renseigne un fichier .json homonyme + un logo homonyme si possible
 * @param string $zip_file
 * @param string $source_url
 * @param bool $force
 * @return string
 */
function debardeur_analyser_zip($zip_file, $source_url, $force = false) {

	$infos_file = dirname($zip_file) . '/' . basename($zip_file, '.zip') . '.json';
	if (!file_exists($infos_file)
	  or filemtime($infos_file) < filemtime($zip_file) // ne doit jamais arriver sur les tags git, mais peut arriver sur les zips venant de la zone
		or $force) {

		$dir_tmp = _DIR_DEBARDEUR_TMP . md5($zip_file);
		passthru("unzip -q $zip_file -d $dir_tmp");
		if (!is_dir($dir_tmp)) {
			return "Echec unzip $zip_file";
		}

		// descendre d'un (ou plusieurs) repertoire si on a un repertoire englobant dans le zip
		$working_dir = $dir_tmp;
		$files = glob("$working_dir/*");
		while (count($files) === 1 and $d = reset($files) and is_dir($d)) {
			$working_dir .= "/" . basename($d);
			$files = glob("$working_dir/*");
		}

		$dtd = false;
		$test_dirs = [$working_dir];
		/*
		 * On commente car le contenu du xml n'est pas utilise pour autre chose que genie/maj dans SPIP
		 * et on veut eviter d'avoir 2Mo de xml a downloader
		 *
		if (stripos(basename($zip_file), 'spip-')===0 and is_dir($working_dir.'/ecrire')) {
			$test_dirs[] = $working_dir.'/ecrire';
		}
		*/
		foreach ($test_dirs as $dtd_dir) {
			$files = glob("$dtd_dir/*.xml");
			if (in_array("$dtd_dir/paquet.xml", $files)) {
				$dtd = "paquet";
				break;
			}
			elseif (in_array("$dtd_dir/plugin.xml", $files)) {
				$dtd = "plugin";
				break;
			}
		}

		$infos = array();
		if ($dtd) {
			$infos = debardeur_analyser_xml($dtd, $dtd_dir . "/");
			if ($logo = $infos['logo'] and $ext = $infos['logo_ext']) {
				$logo_dest = dirname($zip_file) . '/' . basename($zip_file, '.zip') . '.' . $ext;
				@copy($logo, $logo_dest);
				$infos['logo'] = $logo_dest;
			}
			$infos['zip'] = $zip_file;
			$infos['lastmodified'] = filemtime($zip_file);
			$infos['lastcommit'] = $infos['lastmodified'];
			$infos['url'] = $source_url;
			// cas particulier des paquets issus de smart-paquet : le zip n'est pas a la date du dernier commit
			// et il faut recuperer la date dans svn.revision
			if (file_exists($f = "$working_dir/svn.revision")) {
				$revision = file_get_contents($f);
				if (preg_match(",<commit>(.*)</commit>,Uims", $revision, $m)
				  and $t = strtotime($m[1])) {
					$infos['lastcommit'] = $t;
				}
				if (preg_match(",<origine>(.*)</origine>,Uims", $revision, $m)
				  and $origine = trim($m[1])) {
					if (strpos($origine, "svn://zone.spip.org/spip-zone/") === 0) {
						$origine = substr($origine, 30);
					}
					$infos['origin'] = $origine;
				}
			}
		}
		else {
			// cas particulier des vieux SPIP < 3 sans xml
			if (stripos(basename($zip_file), 'spip-')===0) {
				$infos = [
					'dtd' => 'spip', // une valeur qui soit pas false pour ne pas etre bloque par l'ecriveur
					'zip' => $zip_file,
					'lastmodified' => filemtime($zip_file),
					'url' => $source_url,
				];
				$infos['lastcommit'] = $infos['lastmodified'];
			}
			else {
				$infos = [
					'dtd' => false,
					'erreur' => "Aucun paquet.xml/plugin.xml"
				];
			}
		}

		file_put_contents($infos_file, json_encode($infos));
		passthru("rm -fR $dir_tmp");
		return basename($zip_file) ." analysé => $infos_file";
	}
	return "";
}


function debardeur_analyser_xml($dtd, $working_dir){

	$traductions = '';
	$logo = '';
	$logo_ext = '';
	$multis = '';
	$xml = false;
	$autorisations = '';

	$xml_file = $working_dir . $dtd . ".xml";

	// Recuperer le xml qui decrit le plugin et suppression de l'entete XML si elle existe
	$re = ",<" . "\?xml[^>]*\?" . ">,Uims";
	$xml = trim(preg_replace($re, '', file_get_contents($xml_file)));

	// Construire la liste des traductions du plugin
	$traductions = debardeur_compiler_traductions($working_dir);

	// Contruire les balises multi du nom, slogan et description qui ne sont plus dans le paquet
	$f = 'empaqueteur_' . $dtd . '_multi';
	$multis = function_exists($f) ? $f($working_dir) : '';

	// Lister les autorisations du plugin
	// -- détermination de la regexp en fonction de la DTD
	$regexp = ($dtd=='plugin')
		? "#<pipeline>\s*<nom>\s*autoriser\s*</nom>\s*<inclure>\s*([\w/_.]*)\s*</inclure>#imU"
		: "#<pipeline\s+nom=['\"]autoriser['\"]\s+inclure=['\"]([\w/_.]*)['\"][^>]*>#imU";
	// -- récupération des autorisations si elle existe
	$autorisations = debardeur_lister_autorisations($regexp, $xml, $working_dir);

	// Creer le logo du paquet
	$f = 'empaqueteur_' . $dtd . '_logo';
	$f = !function_exists($f) ? '' : $f($xml, $working_dir);
	if ($f AND file_exists($f) AND preg_match('/[.]([^.]*)$/', $f, $r)){
		$logo = $f;
		$logo_ext = $r[1];
	}

	return [
		'dtd' => $dtd,
		'xml_file' => $xml_file,
		'xml' => $xml,
		'traductions' => $traductions,
		'multis' => $multis,
		'autorisations' => $autorisations,
		'logo' => $logo,
		'logo_ext' => $logo_ext,
	];
}


/**
 * Fonction de compilation de la liste des traductions d'un plugin sous forme d'une suite
 * de balises <traduction>
 * On considere que les fichiers de langue ou les rapports de salvatore sont toujours dans
 * le sous-repertoire lang/
 *
 * @param string $working_dir
 *   emplacement des sources du plugin
 * @return string
 */
function debardeur_compiler_traductions($working_dir){

	// On charge une fois la liste des codes de langues
	if (empty($GLOBALS['codes_langues'])) {
		include_spip('inc/debardeur_langues');
	}

	$traductions = '';

	// Determination des modules sous salvatore : on cherche les rapports xml
	$modules_salvatore = array();
	if ($rapports = glob($working_dir . 'lang/*.xml')) {
		foreach ($rapports as $_rapport) {
			$modules_salvatore[] = basename($_rapport, '.xml');
			$contenu = file_get_contents($_rapport);
			$traductions .= $contenu;
		}
	}

	// Determination des modules non traduits par salvatore
	// Cette recherche n'est pas totalement deterministe car on est oblige de considerer
	// qu'il existe toujours un fichier module_fr.php pour identifier le nom du module
	if ($fichiers_fr = glob($working_dir . 'lang/*_fr.php')) {
		foreach ($fichiers_fr as $_fichier_fr) {
			$nom_fichier = basename($_fichier_fr, '.php');
			// On exclut les fichiers de traduction du paquet.xml
			if (strpos($nom_fichier, 'paquet-') === false ) {
				$module = substr($nom_fichier, 0, strlen($nom_fichier)-3);
				// Si ce module n'a pas ete traite dans un rapport Salvatore on cherche toutes
				// ses traductions via les fichiers de langue.
				if (
					isset($module)
					&& !in_array($module, $modules_salvatore)
					&& ($fichiers_langue = glob($working_dir . "lang/".$module."_*.php"))
				) {
					$liste_langues = '';
					foreach ($fichiers_langue as $_fichier_langue) {
						$nom_fichier = basename($_fichier_langue, '.php');
						$langue = substr($nom_fichier, strlen($module) + 1 - strlen($nom_fichier));
						// Si la langue est reconnue, on l'ajoute a la liste des traductions
						// Comme on ne connait pas les traducteurs, la balise est donc vide
						if (isset($GLOBALS['codes_langues'][$langue]) && $langue != 'fr') {
							$liste_langues .= "\t" . '<langue code="' . $langue . '" />' . "\n";
						}
					}
					// Le gestionnaire n'est pas precise et la langue de reference est toujours le fr
					$traductions .= '<traduction module="' . $module . '" reference="fr">' . "\n" .
									$liste_langues .
									'</traduction>' . "\n";
				}
			}
		}
	}

	// On inclus les balise <traduction> dans une balise <traductions> sans attribut qui
	// facilite le travail du parser
	if ($traductions)
		$traductions = '<traductions>' . "\n" . $traductions . '</traductions>' . "\n";

	return $traductions;
}


/**
 * Fonction de compilation de la liste des autorisations d'un plugin sous forme d'une suite
 * de balises <autorisation> incluses dans une balise englobantes <autorisations>
 *
 * @param $regexp
 *   regpex de récupération du pipeline autoriser dans le xml
 * @param $xml
 *   contenu de fichier xml du plugin
 * @param $working_dir
 * @return string
 */
function debardeur_lister_autorisations($regexp, $xml, $working_dir) {
	$autorisations = '';

	// Recherche du pipeline autoriser dans le xml du plugin
	if (preg_match($regexp, $xml, $matches)) {
		$fichier_autorisations = $working_dir . $matches[1];
		if (file_exists($fichier_autorisations)) {
			$contenu = file_get_contents($fichier_autorisations);
			if (preg_match_all("%function\s+autoriser_([^\(]*)%ims", $contenu, $matches)) {
				$fonctions = array_map('trim', $matches[1]);
				foreach($fonctions as $_fonction) {
					if (substr($_fonction, -5)=='_dist') {
						$dist = 'oui';
						$nom = substr($_fonction, 0, strlen($_fonction)-5);
					} else {
						$dist = 'non';
						$nom = $_fonction;
					}
					$autorisations .= "\t" . '<autorisation nom="' . $nom . '" dist="' . $dist . '" />' . "\n";
				}
			}
		}
	}

	return ($autorisations ? "<autorisations>\n$autorisations</autorisations>" : '');
}

