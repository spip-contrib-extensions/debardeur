<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Inc
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit le contenu multi des balises nom, slogan et description a partir des items de langue
 * contenus dans les fichiers paquet-prefixe_langue.php
 *
 * @param $working_dir
 * @return string
 */
function empaqueteur_paquet_multi($working_dir) {
	$multis = '';

	if ($fichier_fr = glob($working_dir . 'lang/paquet-*_fr.php')) {
		// Determination du nom du module, du prefixe et des items de langue
		$nom_fr = basename($fichier_fr[0], '.php');
		$prefixe = substr($nom_fr, 7, strlen($nom_fr)-3-7);
		$module = "paquet-$prefixe";
		$item_nom = $prefixe . "_nom";
		$item_slogan = $prefixe . "_slogan";
		$item_description = $prefixe . "_description";

		// On cherche tous les fichiers de langue destines a la traduction du paquet.xml
		if ($fichiers_langue = glob($working_dir . "lang/{$module}_*.php")) {
			$nom = $slogan = $description = '';
			foreach ($fichiers_langue as $_fichier_langue) {
				$nom_fichier = basename($_fichier_langue, '.php');
				$langue = substr($nom_fichier, strlen($module) + 1 - strlen($nom_fichier));
				// Si la langue est reconnue, on traite la liste des items de langue
				if (isset($GLOBALS['codes_langues'][$langue])) {
					$trads = empaqueteur_lire_fichier_langue($_fichier_langue);
					foreach ($trads as $_item => $_traduction) {
						if ($_item === $item_nom) {
							$nom .= "\n[$langue]$_traduction";
						}
						if ($_item === $item_slogan) {
							$slogan .= "\n[$langue]$_traduction";
						}
						if ($_item === $item_description) {
							$description .= "\n[$langue]$_traduction";
						}
					}
				}
			}

			// Finaliser la construction des balises multi
			if ($nom) $multis .= "<nom>\n<multi>$nom\n</multi>\n</nom>\n";
			if ($slogan) $multis .= "<slogan>\n<multi>$slogan\n</multi>\n</slogan>\n";
			if ($description) $multis .= "<description>\n<multi>$description\n</multi>\n</description>\n";
		}
	}

	return ($multis ? "<multis>\n$multis</multis>" : '');
}

/**
 * Lecture des fichiers de langue
 *
 * - format SPIP >= 4.1 en return []
 * - format spip < 5.0 en $GLOBALS
 *
 * @see lire_fichier_langue()
 * @param string $fichier
 * @return array
 */
function empaqueteur_lire_fichier_langue(string $fichier): array {
	$idx_lang_before = $GLOBALS['idx_lang'] ?? null;
	$idx_lang_tmp = ($GLOBALS['idx_lang'] ?? 'lang') . '@temporaire';
	$GLOBALS['idx_lang'] = $idx_lang_tmp;
	$idx_lang = include $fichier;
	$GLOBALS['idx_lang'] = $idx_lang_before;
	if (!is_array($idx_lang)) {
		if (isset($GLOBALS[$idx_lang_tmp]) and is_array($GLOBALS[$idx_lang_tmp])) {
			$idx_lang = $GLOBALS[$idx_lang_tmp];
		} else {
			$idx_lang = [];
		}
		unset($GLOBALS[$idx_lang_tmp]);
	}
	return $idx_lang;
}


// Renvoie le path complet du logo a partir de la balise icon de plugin.xml et de la racine des sources
function empaqueteur_paquet_logo($xml, $working_dir) {
	if (preg_match('#logo\s*=\s*[\'"](.+)[\'"]#i', $xml, $matches)) {
		$logo = $working_dir . trim($matches[1]);

		$logo_svg = preg_replace(",-\d+\.(png|svg)$,", "-xx.svg", $logo);
		if ($logo_svg !== $logo and file_exists($logo_svg)) {
			return $logo_svg;
		}

		return $logo;
	}
  return '';
}
