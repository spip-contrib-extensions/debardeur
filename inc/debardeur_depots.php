<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Inc
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Creer le repertoire du depot
 * @param $dir_depots
 * @param $depot
 * @return string
 */
function debardeur_depot_preparer($dir_depots, $depot) {

	$dir = $dir_depots . preg_replace(",[^\w-],","_", $depot);
	if (!is_dir($dir)) {
		passthru("mkdir -p $dir");
	}


	return "$dir/";
}

/**
 * Ajouter tous les zips d'un projet au depot logique
 * @param string $dir_depot
 * @param string $dir_source
 * @param array $source
 * @return int
 */
function debardeur_depot_ajouter_source($dir_depot, $dir_source, $source) {
	$info_files = glob($dir_source . "*.json");

	$dir_target = $dir_depot;
	if ($dir_dest = $source['dir_dest']) {
		$dir_target .= $dir_dest . '/';
		passthru("mkdir -p $dir_target");
	}

	// on peut avoir un slug vide si explicite (ie pour les paquets de smart-paquet)
	$racine_fichiers = $dir_target . ($source['slug'] ? $source['slug'] . "-" : '');

	$nb_files = 0;

	foreach ($info_files as $info_file) {

		$info = file_get_contents($info_file);
		if ($info = json_decode($info, true)
		  and $info['dtd']
		  and empty($info['erreur'])) {

			$links = [
				realpath($info['zip']) => $racine_fichiers . basename($info['zip']),
				// on ne link pas le json pour avoir un depot propre, le compileur ira le chercher en suivant le lien symbolique du zip
				//realpath($info_file) => $racine_fichiers . basename($info_file),
			];
			if (isset($info['logo']) and $info['logo']) {
				$links[realpath($info['logo'])] = $racine_fichiers . basename($info['logo']);
			}

			$ajoute = 0;
			foreach($links as $source => $dest) {
				$res = debardeur_check_or_update_symlink($source, $dest);
				if ($res === false) {
					debardeur_fail("Erreur creation symlink", "$source => $dest");
				}
				$ajoute+=$res;
			}

			if ($ajoute) {
				$nb_files++;
			}
		}

	}

	return $nb_files;
}


function debardeur_depot_compter_zips($dir, &$out = "", $prof = 0) {
	if ($prof>10) {
		$dir_source = $dir;
		while($prof-->0) {
			$dir_source = dirname($dir_source);
		}
		debardeur_fail("Recursion infinie", "Fonction debardeur_depot_compter_zips : Recursion infinie dans le repertoire $dir_source ?");
	}
	$dir = rtrim($dir,'/') . '/';
	$subdir = glob($dir . "*", GLOB_ONLYDIR);
	$total = 0;
	$subout = "";
	foreach ($subdir as $d) {
		$total += debardeur_depot_compter_zips($d, $subout, $prof+1);
	}
	$zips = glob($dir . "*.zip");
	$nb = count($zips);
	$total += $nb;
	$out .= str_pad("", 2 * $prof, " ") . basename(rtrim($dir,'/')) . ($nb ? ": $nb zips" : '') ."\n"
		. $subout;
	return $total;
}

/**
 * Nettoyer tous les liens symboliques cassés dans un depot
 * (source supprimee par exemple)
 * @param $dir_depot
 */
function debardeur_dir_nettoyer_liens_morts($dir, $recurs = true) {
	$cleaned = [];
	if ($recurs) {
		$subdirs = glob($dir . "*", GLOB_ONLYDIR);
		foreach ($subdirs as $subdir) {
			$cleaned = array_merge($cleaned, debardeur_dir_nettoyer_liens_morts("$subdir/"));
		}
	}

	// glob ne retourne pas les liens morts :(
	$files = [];
	exec("ls $dir",$files);
	foreach ($files as $file) {
		if ($file) {
			$file = $dir . $file;
			if (is_link($file)
				and (!$target = readlink($file)
					or realpath($target) === false
				  or !file_exists($target))) {
				@unlink($file);
				$cleaned[] = $file;
			}
		}
	}
	return $cleaned;
}

/**
 * Lister les fichiers d'un repertoire selon un pattern
 * @param string $dir
 * @param string $pattern
 * @param bool $recurs
 * @return array|false
 */
function debardeur_dir_lister_fichiers($dir, $pattern, $recurs = true) {
	$files = glob($dir . $pattern);

	if ($recurs) {
		$subdirs = glob($dir . "*", GLOB_ONLYDIR);
		foreach ($subdirs as $subdir) {
			$files = array_merge($files, debardeur_dir_lister_fichiers("$subdir/", $pattern));
		}
	}

	return $files;
}

/**
 * Compiler toutes les infos de tous les zips dans un archive.xml du depot
 * @param $dir_depot
 * @throws Exception
 * @return array
 */
function debardeur_depot_compiler($dir_depot) {

	$cleaned = debardeur_dir_nettoyer_liens_morts($dir_depot);

	$archives_xml = [];
	$traductions = [];
	$logo_files = [];

	// On complete les archives avec les informations du depot
	if ($template = find_in_path("base/template-depot-" . basename($dir_depot) . ".xml")) {
		$xml_depot = file_get_contents($template);
		$xml_depot = trim($xml_depot);
		if (strlen($xml_depot)) {
			$xml_depot .= "\n";
		}
	} else {
		$xml_depot = '';
	}

	$is_depot_spip = (basename($dir_depot) === 'spip' ? true : false);
	$zip_files = debardeur_dir_lister_fichiers($dir_depot, '*.zip');
	$nb_archives = 0;
	if (count($zip_files)) {

		$zip_files = debardeur_trier_zips_par_version($zip_files, $is_depot_spip ? false : true, $is_depot_spip ? true : false);

		$filetmp = _DIR_DEBARDEUR_TMP .  "archives.xml.". getmypid() .".tmp";
		file_put_contents($filetmp, $xml_depot . "<archives>\n");

		$source_deja_vue = [];

		foreach ($zip_files as $zip_file) {

			$info_json = readlink($zip_file);
			$info_json = dirname($info_json) . '/' . basename($info_json, '.zip') . '.json';

			if (!file_exists($info_json)
			  or !$infos = file_get_contents($info_json)
			  or !$infos = json_decode($infos, true)) {
				debardeur_fail("Erreur lecture JSON", "Fichier $zip_file : $info_json vide on corrompu");
			}

			$logo_file = '';
			if (
				!empty($infos['logo_ext'])
				and $infos['logo']
			) {
				$logo_file = dirname($zip_file) . "/" . basename($zip_file, '.zip') . '.' . $infos['logo_ext'];
			}
			if ($logo_file and !file_exists($logo_file)) {
				debardeur_fail("Erreur lecture JSON", "Fichier $zip_file sans logo associé $logo_file");
			}

			// on passe tout en relatif au dossier du depot
			$zip_file = substr($zip_file, strlen($dir_depot));
			if ($logo_file) {
				$logo_file = substr($logo_file, strlen($dir_depot));
				$logo_files[$logo_file] = $zip_file;
			}

			$source = (isset($infos['origin']) ? $infos['origin'] : $infos['url']);
			$xml = debardeur_depot_archive_xml($infos, $zip_file, $logo_file, empty($source_deja_vue[$source]) ? true : false);
			$source_deja_vue[$source] = true;
			file_put_contents($filetmp, $xml . "\n", FILE_APPEND);
			$nb_archives++;
		}
	}

	if (!$nb_archives) {
		@unlink($filetmp);
		return [false, $cleaned];
	}

	file_put_contents($filetmp, "\n</archives>\n", FILE_APPEND);

	$file_xml = $dir_depot . "archives.xml";
	if (!file_exists($file_xml) or md5_file($filetmp) !== md5_file($file_xml)) {
		@unlink($file_xml);
		@rename($filetmp, $file_xml);
	}
	else {
		@unlink($filetmp);
	}

	debardeur_indexer_logos($dir_depot . "logos.php", $logo_files);
	return [$nb_archives, $cleaned];
}

/**
 * Trier les zips par version decroissante
 * @param array $zip_files
 * @param bool $last_y_only
 * @param bool $archives_only
 * @return array
 */
function debardeur_trier_zips_par_version($zip_files, $last_y_only = false, $archives_only = false) {
	$classified_zip = [];
	$dev_flags = ['master','rc','alpha', 'beta', 'dev'];

	$has_version_4_digits = [];
	foreach ($zip_files as $zip_file) {
		// pour le depot SPIP on ne reference que les zips de archives/ et pas les version dev/alpha/beta/rc
		if ($archives_only) {
			if (basename(dirname($zip_file)) !== 'archives') {
				// on ignore ce zip
				continue;
			}
			foreach ($dev_flags as $dev_flag) {
				if (strpos(basename($zip_file), '-'.$dev_flag)) {
					// on ignore ce zip
					continue 2;
				}
			}
		}
		$dir = dirname($zip_file);
		$basename = basename($zip_file, '.zip');
		$family = $basename;
		$version_id = 0;
		if (preg_match(",-v?(\d+)(\.(\d+))?(\.(\d+))?(\.(\d+))?$,i", $basename, $matches)) {
			$family = substr($basename,0, -strlen($matches[0]));
			// X00Y00Z00a0n
			$version_id = intval(((($matches[1] * 1000 + ($matches[3] ?? 0)) * 1000 + ($matches[5] ?? 0)) * 1000 + ($matches[7] ?? 0)) * 100);
			if (!empty($matches[7])) {
				$has_version_4_digits[$family] = true;
			}
			while(isset($classified_zip[$dir][$family][$version_id])) {
				$version_id++;
			}
		}
		$classified_zip[$dir][$family][$version_id] = $zip_file;
	}

	$sorted_zip = [];
	foreach ($classified_zip as $dir => $families_zip) {
		foreach ($families_zip as $family => $versions_zip){
			krsort($versions_zip);
			$seen = [];
			$truncation_length = (empty($has_version_4_digits[$family]) ? 6 : 9);
			foreach ($versions_zip as $version_id => $zipfile) {
				// 00X00Y00Z00a0n
				$version_id = str_pad($version_id, 14, "0", STR_PAD_LEFT);
				$version_y = substr($version_id, 0, $truncation_length);
				if ($last_y_only === false or !isset($seen[$version_y])){
					//var_dump("$version_y:$zipfile");
					$sorted_zip[] = $zipfile;
					$seen[$version_y] = true;
				}
			}
		}
	}

	return $sorted_zip;
}

function debardeur_depot_archive_xml($infos, $zip_file, $logo_file, $complet_with_traductions = true) {

	$dtd = $infos['dtd'];

	// Bloc d'info du paquet
	// pour les archive SPIP il faut garder l'extension .zip pour marcher la regexp du genie maj
	// <archive id="archives/spip-v3.2.11.zip" dtd="spip">
	// pour les autres on nettoie l'id
	$id = $zip_file;
	if ($dtd !== 'spip') {
		$id = preg_replace(",\.zip$,", "", $id);
	}
	$file_size = filesize($infos['zip']);
	$lastmodified = $infos['lastmodified'];
	$source = (isset($infos['origin']) ? $infos['origin'] : $infos['url']);
	$lastcommit = (isset($infos['lastcommit']) ? $infos['lastcommit'] : $infos['lastmodified']);
	$date_last_commit = date('Y-m-d H:i:s', $lastcommit);

	// 31/03/2020 : on enleve les autorisations pour alleger le xml final en attendant mieux
	// et on ne mets les traduction que sur le paquet la derniere version
	$meta_xml = [];
	if (isset($infos['xml'])) {
		$meta_xml[] = $infos['xml'];
	}
	if (isset($infos['multis'])) {
		$meta_xml[] = $infos['multis'];
	}
	/*, $infos['autorisations']*/
	
	if ($complet_with_traductions and isset($infos['traductions'])) {
		array_unshift($meta_xml, $infos['traductions']);
	}
	$meta_xml = array_map('trim', $meta_xml);
	$meta_xml = array_filter($meta_xml);
	$meta_xml = implode("\n", $meta_xml);

	$archive_xml = "<archive id=\"$id\"" . ($dtd ? " dtd=\"$dtd\"" : "") . ">
<zip>
<file>$zip_file</file>
<size>$file_size</size>
<date>$lastmodified</date>
<source>$source</source>
<last_commit>$date_last_commit</last_commit>
<logo>$logo_file</logo>
</zip>
$meta_xml
</archive>\n\n";

	return $archive_xml;

/*
	// deprecated : recuperer les infos de traduction pour reconstruire le fichier traductions.txt
	// on a pas ce qu'il faut ici : on ne sait pas que quelle branche il faut connecter salvatore
	// Bloc d'info pour Salvatore si il existe
	if (preg_match_all("#<(salvatore|traduction)\s+module=['\"](\w*)['\"]\s+reference=['\"](\w*)['\"]\s*"."/>#i", $descr_xml, $matches)) {
		foreach ($matches[2] as $_i => $_module) {
			$salvatore .= rtrim($depot['url_serveur'], '/') . '/' .	rtrim($source, '/') . '/lang/;' .
						  $_module . ';' .
						  $matches[3][$_i] . "\n";
		}
	}
	if($salvatore){
	// On ajoute une en-tete au fichier genere
	$salvatore =
"# LISTE DES PLUGINS / SQUELETTES UTILISANT SALVATORE
# --------------------------------------------------
# Depot : " . $depot['titre'] . "
# Generation par Smart-Paquets le " . date('d-m-Y H:i') . "
#\n" . $salvatore;
	}
*/
}


//
//
//
// Le fichier est recree systematiquement
//
// $nom_fichier	:
// $dir_paq		: repertoire de depot des paquets crees
// $dir_repo	: repertoire racine des sources extraits du repository
//
// return		: aucun
/**
 * Fonction creant le fichier index des logos permettant d'afficher une page
 * de controle des logos
 *
 * @param string $nom_fichier
 *   nom du fichier index des logos
 * @param array $logo_files
 */
function debardeur_indexer_logos($nom_fichier, $logo_files) {

	$items = [];
	foreach ($logo_files as $logo_file => $zip_file) {
		$items[] = "<div class='logo'><img src='$logo_file' title='$zip_file'/></div>";
	}

	if (!count($items)) {
		return;
	}

	$nb = count($items);

	$items = implode("\n", $items);
	$contenu = <<<html
<html>
<head>
	<title>Index des Logos</title>
	<style type="text/css">
	.gallerie {
		display: flex;
		flex-wrap: wrap;
	}
	.gallerie .logo {
		padding:5px;
		margin:5px;
		background: #eee;
		max-width: 32px;
		width: auto;
		display: flex;
		align-items: center;
	}
	.gallerie .logo img {
		max-width: 100%;
		height: auto;
		margin: auto;
	}
</style>
</head>
<body>
<h1>$nb logos</h1>
<div class="gallerie">
$items
</div>
</body>
</html>
html;

	file_put_contents($nom_fichier, $contenu);
}
