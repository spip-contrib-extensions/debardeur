<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Inc
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * initialiser debardeur si besoin
 * peut etre appelle plusieurs fois
 * @param string|array $log_function
 * @param bool $display_time
 * @throws Exception
 */
function debardeur_init($log_function = null, $display_time = false){
	static $initialized;

	// set log function if any
	if ($log_function){
		debardeur_log('', $log_function, $display_time);
	}

	if (is_null($initialized)){
		if (!defined('_DEBUG_DEBARDEUR')){
			define('_DEBUG_DEBARDEUR', 1); // undef si on ne veut pas de messages
		}

		if (!defined('_DIR_DEBARDEUR')){
			define('_DIR_DEBARDEUR', _DIR_RACINE . 'debardeur/');
		}

		if (!defined('_DIR_DEBARDEUR_TMP')){
			define('_DIR_DEBARDEUR_TMP', _DIR_DEBARDEUR . 'tmp/');
		}

		// le repertoire ou l'on stocke les sources (zips)
		if (!defined('_DIR_DEBARDEUR_SOURCES')){
			define('_DIR_DEBARDEUR_SOURCES', _DIR_DEBARDEUR . 'sources/');
		}

		// le repertoire ou l'on stocke les depots svp
		if (!defined('_DIR_DEBARDEUR_DEPOTS')){
			define('_DIR_DEBARDEUR_DEPOTS', _DIR_DEBARDEUR . 'depots/');
		}

		// le repertoire ou l'on stocke les depots svp
		if (!defined('_DIR_DEBARDEUR_ARCHIVELISTS')){
			define('_DIR_DEBARDEUR_ARCHIVELISTS', _DIR_DEBARDEUR . 'archivelists/');
		}

		// verifications des repertoires
		foreach ([_DIR_DEBARDEUR, _DIR_DEBARDEUR_ARCHIVELISTS, _DIR_DEBARDEUR_SOURCES, _DIR_DEBARDEUR_DEPOTS, _DIR_DEBARDEUR_TMP] as $dir){
			debardeur_check_dir($dir);
		}
		$initialized = true;
	}
}


/**
 * Preparer le fichier archivelist
 * - soit c'est un fichier existant
 * - soit c'est une URL dont on sait extraire une liste de repos
 * Si l'url comporte une ancre, l'ancre est utilisee comme sous-dossier du depot
 * @param $archivelist
 * @return bool|string
 * @throws Exception
 */
function debardeur_preparer_fichier_archivelist($archivelist) {

	if (!$archivelist) {
		$archivelist = _DIR_DEBARDEUR_ARCHIVELISTS . 'archivelist.txt';
	}

	if (strpos($archivelist, '://') !== false) {
		$parts = explode('#', $archivelist, 2);
		$url = array_shift($parts);
		$subdir = "";
		if (count($parts)) {
			$subdir = array_shift($parts);
			$subdir = preg_replace(",[^\w-],", "_", $subdir);
		}
		if ($connecteur = debardeur_trouver_connecteur('git', $url)
		  and $connecteur !== 'git'
		  and $lister_repositories = debardeur_connecteur_function($connecteur, 'lister_repositories')) {

			$repositories = $lister_repositories($url);
			if ($repositories === false) {
				return false;
			}
			$archivelist_file = _DIR_DEBARDEUR_ARCHIVELISTS . "auto-" . basename($url) . "-" . substr(md5($archivelist), 0, 5) . ".txt";
			$contenu = "";

			foreach($repositories as $repository) {
				$contenu .= "git;" . $repository['url'] . ($subdir ? ";$subdir" : "") . "\n";
			}

			file_put_contents($archivelist_file, $contenu);
			return $archivelist_file;

		}

		return false;
	}

	if (file_exists($archivelist)) {
		return $archivelist;
	}

	return false;
}

/**
 * chargement d'un fichier archivelist.txt
 * Construit une liste de sources a debarder
 *
 * @param string $fichier_archivelist
 * @return array
 * @throws Exception
 */
function debardeur_charger_fichier_archivelist($fichier_archivelist = null){

	debardeur_init();
	if (is_null($fichier_archivelist)){
		$fichier_archivelist = _DIR_DEBARDEUR_ARCHIVELISTS . 'archivelist.txt';
	}
	debardeur_check_file($fichier_archivelist);

	$lignes = file($fichier_archivelist);
	$lignes = array_map('trim', $lignes);
	$lignes = array_filter($lignes);

	$liste_sources = array();
	foreach ($lignes as $ligne){
		if ($ligne[0]!=='#'){
			$liste = explode(';', trim($ligne));
			$methode = $url = $dir_dest = '';

			// format 2 ou 3 valeurs :
			// l'argument dir_dest est un eventuel sous-dossier dans le depot (le zip est dans un sous-dossier du depot)
			// methode;url[;dir_dest]
			// svn;url
			// git;url
			$methode = array_shift($liste);
			$url = array_shift($liste);
			if (count($liste)) {
				$dir_dest = array_shift($liste);
			}

			$methode = trim($methode);
			$url = trim($url);
			$url = rtrim($url, '/'); // homogeneiser

			if ($methode
				and $url){

				// unifier les urls git en https, plus simple a gerer car ne necessitent pas une cle ssh sur le user php (www-data)
				if (strpos($url, "git@git.spip.net:") === 0) {
					$url = "https://git.spip.net/" . substr($url, 17);
				}

				// si l'url finit par une ancre, c'est le slug (prefixe des zips) qui est explicite
				// easter egg feature pour les zips de smart-paquet qu'on ne veut pas prefixer : on met juste un # sans rien d'autre (== pas de slug)
				$slug = null;
				$d = explode('#', $url, 2);
				if (count($d) == 2) {
					list($url, $slug) = $d;
					$url = rtrim($url, '/'); // homogeneiser
				}

				// definir un dir checkout unique meme si plusieurs modules de meme nom dans differents repos
				$d = explode('/', $url);
				$source = array_pop($d);
				while (count($d) and in_array($source, ['', 'lang', 'trunk', 'ecrire'])){
					$source = array_pop($d);
				}
				$source = basename($source, '.git');

				$hash = substr(md5("$methode:$url"), 0, 5);
				if (!is_null($slug)) {
					$slug = preg_replace(',[^\w-],', '_', trim($slug));
				}
				else {
					$slug = preg_replace(',[^\w-],', '_', trim($source)) . '-' . $hash;
				}
				$dir_checkout = preg_replace(",\W+,", "-", "$methode-$url") . "-" . $hash;

				$dir_dest = rtrim($dir_dest, '/');

				$liste_sources[] = [
					'methode' => $methode,
					'url' => $url,
					'short' => $source,
					'slug' => $slug,
					'dir_checkout' => $dir_checkout,
					'dir_dest' => $dir_dest,
				];
			} else {
				debardeur_log("Fichier $fichier_archivelist, IGNORE ligne incomplete : $ligne");
			}
		}
	}

	return $liste_sources;
}


/**
 * Filtrer la liste complete pour ne garder que une ou plusieurs sources specifiques
 * @param array $liste_trad
 * @param string|array $modules
 * @return array
 */
function debardeur_filtrer_liste_sources($liste_sources, $filter) {
	$liste_filtree = [];
	$exclude = [];
	if (is_string($filter)) {
		if ($filter === 'updated') {
			$filter = debardeur_lister_updated_sources();
		}
		else {
			$filter = explode(',', $filter);
		}
	}
	$keep = array_map('trim', $filter);

	if (!count($keep)) {
		return $liste_filtree;
	}

	if (strpos(reset($keep), 'exclude:') === 0 or strpos(reset($keep), 'exclure:') === 0){
		$exclude = $keep;
		$keep = [];
		$first = array_shift($exclude);
		$first = trim(substr($first,8));
		$exclude[] = $first;
	}

	foreach ($liste_sources as $source) {
		if (count($keep)) {
			if (in_array($source['short'], $keep) or in_array($source['url'], $keep)) {
				$liste_filtree[] = $source;
			}
		}
		elseif(count($exclude)) {
			if (!in_array($source['short'], $exclude) and !in_array($source['url'], $exclude)) {
				$liste_filtree[] = $source;
			}
		}
	}
	return $liste_filtree;
}

/**
 * Lister les repositories qui ont ete mis a jour : le hook debardeur a pose un json dans tmp/updated
 * @return array
 */
function debardeur_lister_updated_sources() {
	$sources = [];
	if (is_dir($dir = _DIR_DEBARDEUR_TMP . 'updated-tags/')) {
		$files = glob($dir . '*.json');
		foreach ($files as $file) {
			$json = file_get_contents($file);
			if ($json = json_decode($json, true) and !empty($json['url'])) {
				$sources[] = $json['url'];
			}
		}
	}
	return $sources;
}


/**
 * URL du gestionnaire trad-lang exportee dans les xml
 * @return mixed
 */
function debardeur_get_self_url() {
	$url_gestionnaire = $GLOBALS['meta']['adresse_site'];
	if (defined('_DEBARDEUR_TEST_URL_GESTIONNAIRE')) {
		$url_gestionnaire = _DEBARDEUR_TEST_URL_GESTIONNAIRE;
	}
	return $url_gestionnaire;
}

/**
 * Ajouter les credentials user/pass sur les urls de repo
 * @param string $methode
 * @param string $url_repository
 * @return string
 */
function debardeur_set_credentials($methode, $url_repository){
	global $domaines_exceptions, $domaines_exceptions_credentials,
	       $SVNUSER, $SVNPASSWD,
	       $GITUSER, $GITPASSWD;

	// on ne sait pas mettre des credentials si c'est du ssh
	if (strpos($url_repository, '://')!==false){
		$user = $pass = false;
		$parts = parse_url($url_repository);
		if (empty($parts['user']) and empty($parts['pass'])){
			$host = $parts['host'];
			if (file_exists(_DIR_ETC . 'debardeur_passwd.inc')) {
				include_once(_DIR_ETC . 'debardeur_passwd.inc');
			}

			if (!empty($domaines_exceptions)
				and is_array($domaines_exceptions)
				and in_array($host, $domaines_exceptions)){
				// on est dans une exception

				/**
				 * Est-ce que cette exception dispose de credentials (Github?)
				 */
				if (is_array($domaines_exceptions_credentials)
					and !empty($domaines_exceptions_credentials[$host])){
					$user = $domaines_exceptions_credentials[$host]['user'];
					$pass = $domaines_exceptions_credentials[$host]['pass'];
				}

			} else {
				// un truc perso pour un module en particulier ?
				if ($methode==='svn' and isset($SVNUSER)) {
					$user = $SVNUSER;
					$pass = $SVNPASSWD;
				} elseif ($methode==='git' and isset($GITUSER)) {
					$user = $GITUSER;
					$pass = $GITPASSWD;
				}
			}

			if ($user and $pass){
				$url_repository = str_replace("://$host", "://" . urlencode($user) . ":" . urlencode($pass) . "@$host", $url_repository);
			}
		}

	}

	return $url_repository;
}

/**
 * Trouver un connecteur connu pour la methode/url
 * @param string $methode
 * @param string $url
 * @return string
 */
function debardeur_trouver_connecteur($methode, $url) {
	static $known_connector = [];

	$repo_spip_git = "https://git.spip.net/";
	if ($methode === 'git' and strpos($url, $repo_spip_git) === 0) {
		if (!empty($known_connector[$repo_spip_git])) {
			return $known_connector[$repo_spip_git];
		}
		include_spip('inc/distant');
		$res = recuperer_url($repo_spip_git, ['methode' => 'HEAD']);
		if (!empty($res['headers']) and strpos($res['headers'], "\nX-Gitlab") !== false) {
			return $known_connector[$repo_spip_git] = 'gitlab';
		}
		return 'gitea';
	}
	//if (strpos($url, "https://github.com/") === 0) {
	//	return 'github';
	//}

	return $methode;
}


/**
 * Charger une fonction du connecteur
 * @param string $connecteur (git|svn|gitea|github|gitlab)
 * @param string $function
 * @return string
 * @throws Exception
 */
function debardeur_connecteur_function($connecteur, $function) {
	include_spip('debardeur/connecteur/' . $connecteur);
	if (function_exists($f = "debardeur_connecteur_{$connecteur}_$function")
	  or function_exists($f = $f . '_dist')) {
		return $f;
	}
	throw new \Exception("Erreur fonction $f inexistante");
}


/**
 * Appel d'une l'API JSON en get (type gitea/github)
 *
 * @param string $type
 * @param string $endpoint
 * @param string $method
 * @param string|array $query
 * @param null $last_modified_time
 * @return false|mixed|string
 */
function debardeur_json_api_call($type, $endpoint, $method, $query="", $last_modified_time = null){
	$res = debardeur_json_api_call_raw($type, $endpoint, $method, $query, $last_modified_time);

	if ($res and $res['status']){
		return $res['content'];
	}
	return false;
}

/**
 * Appels multiples d'une API JSON en get (type gitea/github) pour recuperer toutes les pages de resultats
 *
 * @param int $nb_pages
 *   nombre maxi de de pages
 * @param string $type
 * @param string $endpoint
 * @param string $method
 * @param string|array $query
 * @param null $last_modified_time
 * @return false|array
 */
function debardeur_json_api_call_pages($nb_pages, $type, $endpoint, $method, $query="", $last_modified_time = null){
	$res = debardeur_json_api_call_raw($type, $endpoint, $method, $query, $last_modified_time);
	$nb = 1;

	if ($res and $res['status']){
		$results = $res['content'];
		$links = debardeur_json_api_extract_links($res['header']);
		while (($nb_pages<=0 or $nb<$nb_pages) and $links and !empty($links['next'])) {
			$q_page = explode('?', $links['next'], 2);
			$q_page = end($q_page);
			// s'assurer que les pages suivantes sont au moins aussi recentes
			if (!empty($res['last_modified'])) {
				$last_modified_time = $res['last_modified'];
			}
			$res = debardeur_json_api_call_raw($type, $endpoint, $method, $q_page, $last_modified_time);
			$nb++;

			$links = false;
			if ($res and $res['status']){
				while(count($res['content'])) {
					$results[] = array_shift($res['content']);
				}
				$links = debardeur_json_api_extract_links($res['header']);
			}
		}
		return $results;
	}
	return false;

}

/**
 * Extraire les links du header Link: de la pagination des resultats
 * @param array $headers
 * @return array
 */
function debardeur_json_api_extract_links($headers) {
	// "Link":"<https:\/\/git.spip.net\/api\/v1\/orgs\/spip-contrib-squelettes\/repos?limit=50&page=2>; rel=\"next\",<https:\/\/git.spip.net\/api\/v1\/orgs\/spip-contrib-squelettes\/repos?limit=50&page=4>; rel=\"last\""
	$links = [];
	if (!empty($headers['link'])) {
		$list = explode(',', $headers['link']);
		foreach ($list as $l) {
			$l = explode(";", $l, 2);
			if (count($l) == 2) {
				$url = reset($l);
				$url = ltrim($url, '<');
				$url = rtrim($url, '>');
				$rel = explode("rel=", end($l));
				$rel = trim(end($rel), '"');
				if ($rel and $url) {
					$links[$rel] = $url;
				}
			}
		}
	}
	return $links;
}

/**
 * Appel d'une l'API JSON en get (type gitea/github) avec cache si possible
 * (mais par defaut pas de cache)
 *
 * @param string $endpoint
 * @param string $method
 * @param string|array $query
 * @param null $last_modified_time
 * @return array
 */
function debardeur_json_api_call_raw($type, $endpoint, $method, $query="", $last_modified_time = null){
	$dir_cache = sous_repertoire(_DIR_DEBARDEUR_TMP, 'cache');
	$dir_cache = sous_repertoire($dir_cache, $type);

	if (is_array($query)){
		$query = http_build_query($query);
	}
	if ($query){
		$query = '?' . ltrim($query, '?');
	} else {
		$query = '';
	}

	$file_cache = $dir_cache . "api-" . md5("debardeur_json_api_call_raw:$endpoint:$method:$query") . ".json";

	if (!is_null($last_modified_time)
		and file_exists($file_cache)
		and filemtime($file_cache)>=$last_modified_time
		and $res = file_get_contents($file_cache)
		and ($res = json_decode($res, true))!==false){
		return $res;
	}

	$url = $endpoint . $method . $query;

	spip_log("debardeur_json_api_call_raw: curl $url", 'debardeur' . _LOG_DEBUG);
	//var_dump("curl $url");

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-Debardeur');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: application/json'));
	curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	curl_setopt($ch, CURLOPT_VERBOSE, 0);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);

	$output = curl_exec($ch);
	$err = curl_errno($ch);
	$errmsg = curl_error($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	list($header, $content) = explode("\r\n\r\n", $output, 2);
	$headers = [];
	$header_lines = explode("\n", $header);
	foreach ($header_lines as $header_line){
		$header_line = explode(':', $header_line, 2);
		if (count($header_line)==2){
			list($k, $v) = $header_line;
			$headers[$k] = trim($v);
		}
	}

	$json = json_decode($content, true);
	$res = [
		'status' => false,
		'header' => $headers,
		'content' => $json,
		'last_modified' => time(),
	];

	if ($httpCode==200 and $json!==false and !is_null($json)){
		$res['status'] = true;
		file_put_contents($file_cache, json_encode($res));
	} else {
		spip_log("Echec call API $url:" . json_encode($res), 'debardeur_call_api' . _LOG_ERREUR);
	}

	return $res;
}


/**
 * Appel d'une API en JSON avec des data
 * retour sous forme json ou false en cas d'erreur grave
 *
 * @param $path
 * @param $data
 * @return array|false
 */
function debardeur_json_recuperer_url($url_api, $options = []) {

	defined('_INC_DISTANT_CONNECT_TIMEOUT') || define('_INC_DISTANT_CONNECT_TIMEOUT', 30);
	include_spip('inc/distant');

	if (
		!empty($options['data'])
		&& (empty($options['methode']) || $options['methode'] === 'GET')
	) {
		$options['methode'] = 'POST';
	}

	if (!empty($options['token'])) {
		$headers = [
			"PRIVATE-TOKEN" => $options['token'],
		];
		$options['headers'] = $headers;
	}

	// toujours un payload
	if (!empty($options['data'])) {
		$options['datas'] = "Content-Type: application/json\r\n\r\n" . json_encode($options['data']);
	}

    //spip_log("Options: " . json_encode($options), 'distant');
	$res = recuperer_url($url_api, $options);
	if (empty($res)) {
		spip_log('debardeur_json_api_call: resultat innatendu ' . json_encode($res), 'debardeur' . _LOG_ERREUR);
		return false;
	}

	if (empty($res['page']) or ($json = json_decode($res['page'], true)) === null or !is_array($json)) {
		$res['page'] = (isset($res['page']) ? $res['page'] : '');
		$res['page'] = substr($res['page'], 0, 512);
		spip_log('debardeur_json_api_call: status innatendu ou json mal formaté ' . json_encode($res), 'debardeur' . _LOG_ERREUR);
		return false;
	}
	spip_log('debardeur_json_api_call: reponse json ' . $res['page'], 'debardeur' . _LOG_DEBUG);

	return $json;
}


/**
 * Verifier qu'un repertoire existe
 * @param $dir
 * @throws Exception
 */
function debardeur_check_dir($dir){
	if (!is_dir($dir)){
		throw new Exception("Erreur : le répertoire $dir n'existe pas");
	}
}

/**
 * Verifier qu'un fichier existe
 * @param $file
 * @throws Exception
 */
function debardeur_check_file($file){
	if (!$file or !file_exists($file)){
		throw new Exception("Erreur : Le fichier $file est introuvable");
	}
}

/**
 * Verifier qu'un lien symbolique existe et pointe bien vers la bonne source
 * (sinon le creer/recreer)
 * @param $source
 * @param $dest
 * @return bool
 */
function debardeur_check_or_update_symlink($source, $dest) {
	if (is_link($dest)) {
		if (readlink($dest) === $source) {
			// rien a faire
			return 0;
		}
		unlink($dest);
	}
	if (file_exists($dest)) {
		// pas un symlink ?
		return false;
	}
	symlink($source, $dest);
	return 1;
}


/**
 * Nettoyer d'une source les zips qui correspondent a des tags supprimes
 * @param $dir_source
 * @param $tags
 */
function debardeur_nettoyer_source($dir_source, $tags) {

	$zip_licites = array_column($tags, 'zip');

	$zip_existants = glob($dir_source . '*.zip');

	$zips_to_delete = array_diff($zip_existants, $zip_licites);
	foreach ($zips_to_delete as $zip_to_delete) {

		$file_base = dirname($zip_to_delete) . '/' . basename($zip_to_delete, '.zip');
		@unlink($zip_to_delete);
		foreach(['json', 'png', 'gif', 'jpg', 'svg'] as $ext) {
			if (file_exists($f = $file_base . '.' . $ext)) {
				@unlink($f);
			}
		}

	}

}

/**
 * Loger
 * @param string $msg
 * @param string|array $display_function
 * @param bool $display_time
 */
function debardeur_log($msg = '', $display_function = null, $display_time = false){
	static $function = null;
	static $time_log = null;

	if ($display_function and is_callable($display_function)){
		$function = $display_function;
		$time_log = $display_time;
	}

	if (defined('_DEBUG_DEBARDEUR')
		and _DEBUG_DEBARDEUR
		and $msg){
		if ($time_log) {
			$t = date('Y-m-d H:i:s') . ": ";
			$msg = $t . str_replace("\n", "\n$t", $msg);
		}
		if ($function){
			call_user_func($function, rtrim($msg));
		} else {
			// fallback : utiliser echo mais enlever les balises de formatage symphony
			$msg = str_replace(["<info>", "</info>", "<error>", "</error>", "<comment>", "</comment>", "<question>", "</question>", "</>"], "", $msg);
			echo rtrim($msg) . "\n";
		}
	}
}

/**
 * Echec sur erreur : on envoie un mail si possible et on echoue en lançant une exception
 * @param $sujet
 * @param $corps
 * @throws Exception
 */
function debardeur_fail($sujet, $corps){
	$corps = rtrim($corps) . "\n\n";
	debardeur_envoyer_mail($sujet, $corps);
	throw new Exception($corps);
}

/**
 * @param string $sujet
 * @param string $corps
 */
function debardeur_envoyer_mail($sujet = 'Erreur', $corps = ''){
	if (defined('_EMAIL_ERREURS') and _EMAIL_ERREURS
		and defined('_EMAIL_DEBARDEUR') and _EMAIL_DEBARDEUR){
		$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
		$destinataire = _EMAIL_ERREURS;
		$from = _EMAIL_DEBARDEUR;
		$envoyer_mail($destinataire, $sujet, $corps, $from);
		debardeur_log("Un email a été envoyé à l'adresse : " . _EMAIL_ERREURS . "\n");
	}
}

/**
 * Verifier que la base de debardeur a bien ete mise a jour
 * pour ajouter le dir_module qui est la cle unique a la place de module
 * lancer
 * spip debardeur:upgrade --traductions=...
 * avec le bon fichier de traduction pour mettre à jour la base de debardeur avant de pouvoir lancer a nouveau le lecteur ou l'ecriveur
 */
function debardeur_verifier_base_upgradee() {

	// todo ?

}
