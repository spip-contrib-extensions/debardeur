<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Inc
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie le path complet du logo a partir de la balise icon de plugin.xml et de la racine des sources
 * @param $plugin_xml
 * @param $working_dir
 * @return string
 */
function empaqueteur_plugin_logo($plugin_xml, $working_dir) {
  return !preg_match('#<icon[^>]*>\s*(.+)\s*</icon>#i', $plugin_xml, $matches) ? '' : ($working_dir . trim($matches[1]));
}

