# Debardeur

<https://fr.wikipedia.org/wiki/D%C3%A9bardage>

Le débardeur est en charge de préparer les paquets pour SVP.
Son architecture est inspirée de celle de Salvatore

Contrairement a Smart-paquets qui se nourissait d'une liste unique pour un depôt et traitait tout d'un coup, les opérations sont ici découpées.

Le débardeur ne travaille que sur les tags et ne gère pas l'archivage de branches vivantes d'un projet.

## Préparation de l'environnement

A la racine du site SPIP où est installé le Débardeur, il est nécessaire de créer le répertoire `debardeur/` avec comme sous-dossiers:

- `archivelists/`
- `sources/`
- `depots/`
- `tmp/`

```bash
mkdir -p debardeur/{archivelists,sources,depots,tmp}
```

## Tireur

Le tireur est en charge d'aller chercher les zips ou de les préparer, en se basant sur un fichier archivelist.

### Format du fichier archivelist

Un paquet par ligne, 2 ou 3 valeurs séparées par des `;` ;

```csv
methode;url[;dir_dest]
```

- `methode` : git ou svn (non supporté pour le moment)
- `url` : url du repository
- `dir_dest` : sous-dossier du depot dans lequel mettre le zip

Exemple :

```csv
git;https://github.com/Cerdic/mastodon.git
git;https://git.spip.net/spip-contrib-extensions/commandes.git
```

### Syntaxe

```bash
spip debardeur:tirer --archivelist=debardeur/archivelists/archivelist.txt
spip debardeur:tirer --archivelist=https://git.spip.net/spip-contrib-squelettes
# spip debardeur:tirer --archivelist=https://github.com/spip/spip
```

Si on fournit une URL en archivelist, le debardeur regarde si il sait utiliser un connecteur pour récupérer la liste des projets et construire un fichier archivelist à la volée.

Pour chaque repository le tireur regarde quel connecteur utiliser pour récuperer les zips des tags.

Les connecteurs existants sont :

- gitlab, détecté pour le serveur git.spip.net, qui récupère les tags et les zips par l'API de GitLab
- github, qui récupère les tags et les zips par l'API de Github
- git, le connecteur par défaut, qui checkout le projet en git et génère un zip pour chaque tag

Les zips sont rangés dans un sous-dossier unique (nom du dépôt + hash) de debardeur/sources pour chaque repository.

## Lecteur

Pour chaque repository du archivelist, le lecteur va allez analyser chaque fichier zip de son dossier source et en extraire les informations nécessaires pour construire les informations du paquet au sens de SVP.
Si les informations ne sont pas encore disponibles, le lecteur dezippe dans un dossier temporaire et travaille sur ce dossier.
Il stocke ensuite ces informations dans fichier un json homonyme au fichier zip et extrait également le logo du zip qu'il range à côté du zip, sous le même nom à l'extension près.

```bash
spip debardeur:lire --archivelist=https://git.spip.net/spip-contrib-squelettes
```

## Ecriveur

L'ecriveur va peupler un dépôt SVP avec tous les zips d'un archivelist via des liens symboliques et en respectant, si besoin, les sous-dossiers indiqués dans le fichier archivelist.

```bash
spip debardeur:ecrire --archivelist=https://git.spip.net/spip-contrib-squelettes --depot=spip-zone
```

Si on utilise un format URL, il est possible d'indiquer avec l'ancre un sous-dossier cible du depot dans lequel ecrire

```bash
spip debardeur:ecrire --archivelist=https://git.spip.net/spip-contrib-squelettes#spip-contrib-squelettes --depot=spip-zone
```

## Compileur

Le compileur compile tous les zips d'un dépôt pour obtenir le fichier archives.xml utilisable par SVP.
De par cette organisation séparée entre ecriveur et compileur, on peut alimenter un même dépôt de plusieurs sources.

```bash
spip debardeur:compiler --depot=spip-zone
# =======================================
# COMPILEUR [Génère le fichier archives.xml]
# =======================================
# Générer le archives.xml du dépot spip-zone debardeur/depots/spip-zone/
# 
#    24 paquets dans le dépot
#
```

## One more thing

La commande `debardeur:debarder` permet de faire un cycle complet tirer/lire/ecrire/compiler :

```bash
spip debardeur:debarder --archivelist=https://git.spip.net/spip-contrib-squelettes#spip-contrib-squelettes --depot=spip-zone
```

## Todo

- [x] compiler un depot avec tous les zips/json presents dedans
- [x] generer un archivelist depuis gitea
- [x] renseigner les infos des depots quelque part (dans base/depots.json)
- [ ] creer un connecteur zone qui va chercher les zips dans smart-paquet (sur le meme serveur)  
  Permet de continuer à faire tourner smart-paquets et d'inclure sa production dans le depot spip-zone compile par le debardeur
- [ ] generer un archivelist a partir de celui de la zone -> utilisera le connecteur de la zone
