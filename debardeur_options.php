<?php
/**
 * Options au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
