<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce script va chercher les tags des repository définis dans le fichier archivelists/archivelist.txt
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class DebardeurTirer extends Command {
	protected function configure(){
		$this
			->setName('debardeur:tirer')
			->setDescription('Va chercher les zips des tags des repositories décrits dans le fichier archivelist')
			->addOption(
				'archivelist',
				null,
				InputOption::VALUE_REQUIRED,
				'Chemin vers le fichier archivelist.txt a utiliser [debardeur/archivelists/archivelist.txt]',
				null
			)
			->addOption(
				'filtre',
				null,
				InputOption::VALUE_REQUIRED,
				'Un ou plusieurs projets a traiter (par defaut tous les projets du fichier de archivelist seront traites)',
				null
			)
			->addOption(
				'force-tagslist-update',
				null,
				InputOption::VALUE_NONE,
				'Forcer la mise a jour des tags (quand on utilise une API)',
				null
			)
			->addOption(
				'time',
				null,
				InputOption::VALUE_NONE,
				'Ajouter date/heure sur les sorties pour les logs',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		global $spip_racine;
		global $spip_loaded;

		include_spip('inc/debardeur');
		include_spip('debardeur/tireur');

		$time = $input->getOption('time');
		debardeur_init(array($output, 'writeln'), !!$time);

		debardeur_log("<comment>=======================================</comment>");
		debardeur_log("<comment>TIREUR [Va chercher les zips des tags d'un repo GIT|Gitea|Github... et les depose dans sa copie locale]</comment>");
		debardeur_log("<comment>=======================================</comment>");


		$archivelist = $input->getOption('archivelist');
		$archivelist_file = debardeur_preparer_fichier_archivelist($archivelist);
		if ($archivelist_file === false) {
			throw new Exception("Impossible de trouver une liste de projets a tirer pour $archivelist");
		}

		$liste_sources = debardeur_charger_fichier_archivelist($archivelist_file);
		$n = count($liste_sources);
		debardeur_log("<info>$n projets dans le fichier archivelist $archivelist_file</info>");

		$force_update = $input->getOption('force-tagslist-update');

		$filtre = $input->getOption('filtre');
		if ($sources = trim($filtre)) {
			if ($filtre === 'updated') {
				$force_update = true;
			}
			$liste_sources = debardeur_filtrer_liste_sources($liste_sources, $filtre);
			$n = count($liste_sources);
			debardeur_log("<info>$n projets à traiter : " . $sources . "</info>");
		}

		debardeur_tirer($liste_sources, $force_update);

		return Command::SUCCESS;
	}
}

