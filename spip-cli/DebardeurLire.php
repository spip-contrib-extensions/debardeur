<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Prend tous les zips des projets listes et les analyse pour avoir les infos necessaires a generer le depot SVP
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class DebardeurLire extends Command {
	protected function configure(){
		$this
			->setName('debardeur:lire')
			->setDescription('Prend les fichiers zips des projets de archivelist et les analyse')
			->addOption(
				'archivelist',
				null,
				InputOption::VALUE_REQUIRED,
				'Chemin vers le fichier archivelist.txt a utiliser [debardeur/archivelists/archivelist.txt]',
				null
			)
			->addOption(
				'filtre',
				null,
				InputOption::VALUE_REQUIRED,
				'Un ou plusieurs projets a traiter (par defaut tous les projets du fichier de archivelist seront traites)',
				null
			)
			->addOption(
				'force',
				null,
				InputOption::VALUE_NONE,
				'Forcer la relecture des zips et la mise a jour des infos',
				null
			)
			->addOption(
				'time',
				null,
				InputOption::VALUE_NONE,
				'Ajouter date/heure sur les sorties pour les logs',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		global $spip_racine;
		global $spip_loaded;

		include_spip('inc/debardeur');
		include_spip('debardeur/lecteur');

		$time = $input->getOption('time');
		debardeur_init(array($output, 'writeln'), !!$time);

		debardeur_log("<comment>=======================================</comment>");
		debardeur_log("<comment>LECTEUR [Analyse les fichiers zip et prepare les infos pour le xml]</comment>");
		debardeur_log("<comment>=======================================</comment>");

		$archivelist = $input->getOption('archivelist');
		$archivelist_file = debardeur_preparer_fichier_archivelist($archivelist);
		if ($archivelist_file === false) {
			throw new Exception("Impossible de trouver une liste de projets a titer pour $archivelist");
		}

		$liste_sources = debardeur_charger_fichier_archivelist($archivelist_file);
		$n = count($liste_sources);
		debardeur_log("<info>$n projets dans le fichier archivelist $archivelist_file</info>");

		$filtre = $input->getOption('filtre');
		if ($sources = trim($filtre)) {
			$liste_sources = debardeur_filtrer_liste_sources($liste_sources, $filtre);
			$n = count($liste_sources);
			debardeur_log("<info>$n projets à traiter : " . $sources . "</info>");
		}

		$force = $input->getOption('force');
		debardeur_lire($liste_sources, $force);

		return Command::SUCCESS;
	}
}

