<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Prend tous les zips des projets listes et les analyse pour avoir les infos necessaires a generer le depot SVP
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class DebardeurDebarder extends Command {
	protected function configure(){
		$this
			->setName('debardeur:debarder')
			->setDescription('Fait un cycle complet (tirer/lire/ecrire/compiler) pour un archivelist et un dépot')
			->addOption(
				'depot',
				null,
				InputOption::VALUE_REQUIRED,
				'Nom du depot logique SVP',
				null
			)
			->addOption(
				'archivelist',
				null,
				InputOption::VALUE_REQUIRED,
				'Chemin vers le fichier archivelist.txt a utiliser [debardeur/archivelists/archivelist.txt]',
				null
			)
			->addOption(
				'filtre',
				null,
				InputOption::VALUE_REQUIRED,
				'Un ou plusieurs projets a traiter (par defaut tous les projets du fichier de archivelist seront traites)',
				null
			)
			->addOption(
				'force-tagslist-update',
				null,
				InputOption::VALUE_NONE,
				'Forcer la mise a jour des tags (quand on utilise une API)',
				null
			)
			->addOption(
				'force',
				null,
				InputOption::VALUE_NONE,
				'Forcer la relecture des zips et la mise a jour des infos',
				null
			)
			->addOption(
				'time',
				null,
				InputOption::VALUE_NONE,
				'Ajouter date/heure sur les sorties pour les logs',
				null
			)
			->addUsage('--filtre=saisies')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		global $spip_racine;
		global $spip_loaded;

		include_spip('inc/debardeur');
		include_spip('debardeur/tireur');
		include_spip('debardeur/lecteur');
		include_spip('debardeur/ecriveur');
		include_spip('debardeur/compileur');


		$time = $input->getOption('time');
		debardeur_init(array($output, 'writeln'), !!$time);

		debardeur_log("<comment>=======================================</comment>");
		debardeur_log("<comment>DEBARDER [Traitement complet d'un archivelist pour un depot depot SVP]</comment>");
		debardeur_log("<comment>=======================================</comment>");

		$archivelist = $input->getOption('archivelist');
		$archivelist_file = debardeur_preparer_fichier_archivelist($archivelist);
		if ($archivelist_file === false) {
			throw new Exception("Impossible de trouver une liste de projets a tirer pour $archivelist");
		}

		$liste_sources = debardeur_charger_fichier_archivelist($archivelist_file);
		$n = count($liste_sources);
		debardeur_log("<info>$n projets dans le fichier archivelist " . ($archivelist ? $archivelist : '') . "</info>");

		$force_tagslist_update = $input->getOption('force-tagslist-update');

		$filtre = $input->getOption('filtre') ?? '';
		if ($sources = trim($filtre)) {
			if ($filtre === 'updated') {
				$force_tagslist_update = true;
			}
			$liste_sources = debardeur_filtrer_liste_sources($liste_sources, $filtre);
			$n = count($liste_sources);
			debardeur_log("<info>$n projets à traiter : " . $sources . "</info>");
		}

		$depot = $input->getOption('depot');
		if (!$depot) {
			throw new Exception("Indiquez un depot dans lequel ecrire");
		}
		$depots = explode(',', $depot);
		$depots = array_map('trim', $depots);
		$depots = array_filter($depots);

		$force = $input->getOption('force');

		foreach ($liste_sources as $une_source){
			debardeur_log("\n<comment>--- Source " . $une_source['short'] . " | " . $une_source['slug'] . " | " . $une_source['url'] . "</comment>");
			debardeur_log("<info>TIRER</info>");
			debardeur_tirer([$une_source], $force_tagslist_update);
			debardeur_log("<info>LIRE</info>");
			debardeur_lire([$une_source], $force);
			debardeur_log("<info>ECRIRE</info>");
			foreach ($depots as $depot){
				debardeur_ecrire([$une_source], $depot);
			}
		}

		debardeur_log("\n<comment>--- COMPILER les dépots</comment>");
		debardeur_compiler($depots);

		return Command::SUCCESS;
	}
}

