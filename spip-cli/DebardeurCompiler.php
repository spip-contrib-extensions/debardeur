<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Prend tous les zips des projets listes et les analyse pour avoir les infos necessaires a generer le depot SVP
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class DebardeurCompiler extends Command {
	protected function configure(){
		$this
			->setName('debardeur:compiler')
			->setDescription('Compiler tous les paquets du depot dans un archives.xml')
			->addOption(
				'depot',
				null,
				InputOption::VALUE_REQUIRED,
				'Nom du depot logique SVP',
				null
			)
			->addOption(
				'time',
				null,
				InputOption::VALUE_NONE,
				'Ajouter date/heure sur les sorties pour les logs',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		global $spip_racine;
		global $spip_loaded;

		include_spip('inc/debardeur');
		include_spip('debardeur/compileur');

		$time = $input->getOption('time');
		debardeur_init(array($output, 'writeln'), !!$time);

		debardeur_log("<comment>=======================================</comment>");
		debardeur_log("<comment>COMPILEUR [Génère le fichier archives.xml]</comment>");
		debardeur_log("<comment>=======================================</comment>");

		$depot = $input->getOption('depot');
		if (!$depot) {
			$depots = glob(_DIR_DEBARDEUR_DEPOTS . '*', GLOB_ONLYDIR);
			$depots = array_map('basename', $depots);
		}
		else {
			$depots = explode(',', $depot);
			$depots = array_map('trim', $depots);
			$depots = array_filter($depots);
		}

		if (!count($depots)) {
			throw new Exception("Aucun depot valide");
		}

		debardeur_compiler($depots);

		return Command::SUCCESS;
	}
}

