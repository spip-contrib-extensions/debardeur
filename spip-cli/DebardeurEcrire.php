<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Prend tous les zips des projets listes et les analyse pour avoir les infos necessaires a generer le depot SVP
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class DebardeurEcrire extends Command {
	protected function configure(){
		$this
			->setName('debardeur:ecrire')
			->setDescription('Ajoute les fichiers zips+logos+json des projets de archivelist a un depot')
			->addOption(
				'depot',
				null,
				InputOption::VALUE_REQUIRED,
				'Nom du depot logique SVP',
				null
			)
			->addOption(
				'archivelist',
				null,
				InputOption::VALUE_REQUIRED,
				'Chemin vers le fichier archivelist.txt a utiliser [debardeur/archivelists/archivelist.txt]',
				null
			)
			->addOption(
				'filtre',
				null,
				InputOption::VALUE_REQUIRED,
				'Un ou plusieurs projets a traiter (par defaut tous les projets du fichier de archivelist seront traites)',
				null
			)
			->addOption(
				'time',
				null,
				InputOption::VALUE_NONE,
				'Ajouter date/heure sur les sorties pour les logs',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		global $spip_racine;
		global $spip_loaded;

		include_spip('inc/debardeur');
		include_spip('debardeur/ecriveur');

		$time = $input->getOption('time');
		debardeur_init(array($output, 'writeln'), !!$time);

		debardeur_log("<comment>=======================================</comment>");
		debardeur_log("<comment>ECRIVEUR [Ajoute les fichiers zip+dependances a un depot SVP]</comment>");
		debardeur_log("<comment>=======================================</comment>");

		$archivelist = $input->getOption('archivelist');
		$archivelist_file = debardeur_preparer_fichier_archivelist($archivelist);
		if ($archivelist_file === false) {
			throw new Exception("Impossible de trouver une liste de projets a ecrire pour $archivelist");
		}

		$liste_sources = debardeur_charger_fichier_archivelist($archivelist_file);
		$n = count($liste_sources);
		debardeur_log("<info>$n projets dans le fichier archivelist " . ($archivelist ? $archivelist : '') . "</info>");

		$filtre = $input->getOption('filtre');
		if ($sources = trim($filtre)) {
			$liste_sources = debardeur_filtrer_liste_sources($liste_sources, $filtre);
			$n = count($liste_sources);
			debardeur_log("<info>$n projets à traiter : " . $sources . "</info>");
		}

		$depot = $input->getOption('depot');
		if (!$depot) {
			throw new Exception("Indiquez un depot dans lequel ecrire");

		}

		debardeur_ecrire($liste_sources, $depot);

		return Command::SUCCESS;
	}
}

