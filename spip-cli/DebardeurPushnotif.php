<?php
/**
 * Fonctions au chargement du plugin Débardeur
 *
 * @plugin     Débardeur
 * @copyright  2020
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Debardeur\Debardeur
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce script va chercher les notifications de push recupere de gitea par action/api_debardeur_hook
 * et envoyer un mail a spip-zone-commit pour chaque push
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class DebardeurPushnotif extends Command {

	private $homepage = 'https://git.spip.net/';

	/** @var InputInterface */
	protected $input;
	/** @var OutputInterface */
	protected $output;

	protected function configure(){
		$this
			->setName('debardeur:pushnotif')
			->setDescription('Va chercher les push recu par hook et les transforme en mail pour spip-zone-commit')
			->addOption(
				'time',
				null,
				InputOption::VALUE_NONE,
				'Ajouter date/heure sur les sorties pour les logs',
				null
			)
			->addOption(
				'silent',
				null,
				InputOption::VALUE_NONE,
				'Pas de sortie log si rien a faire',
				null
			)
			->addOption(
				'onlyIRC',
				null,
				InputOption::VALUE_NONE,
				'Uniquement notification IRC',
				null
			)
			->addOption(
				'onlyMail',
				null,
				InputOption::VALUE_NONE,
				'Uniquement notification Mail',
				null
			)
			->addOption(
				'noConsume',
				null,
				InputOption::VALUE_NONE,
				'Conserve les fichiers de data sans les renommer (rejouer le script réutilisera les mêmes fichiers data)',
				null
			)
			->addOption(
				'dry-run',
				null,
				InputOption::VALUE_NONE,
				'Ne rien envoyer par mail ou irc.',
				null
			)
			->addUsage("")
			->addUsage("-v --onlyIRC --noConsume --dry-run")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		global $spip_racine;
		global $spip_loaded;

		include_spip('inc/debardeur');
		include_spip('inc/filtres');
		include_spip('debardeur/tireur');

		$time = $input->getOption('time');
		debardeur_init(array($output, 'writeln'), !!$time);

		$this->input = $input;
		$this->output = $output;

		// options d'exécutions
		$silent = $input->getOption('silent');
		$noConsume = $input->getOption('noConsume');
		$onlyIRC = $input->getOption('onlyIRC');
		$onlyMail = $input->getOption('onlyMail');

		$dir = _DIR_DEBARDEUR_TMP . 'pushed/';
		$pushed_files = glob($dir . "*.json");
		// les vieilles notifs deja traitees
		$pushed_files_sent = glob($dir . "*.json.sent");

		if ($pushed_files or !$silent) {
			debardeur_log("<comment>=======================================</comment>");
			debardeur_log("<comment>PushNotif [Va chercher les push recu par hook et envoi des mails]</comment>");
			debardeur_log("<comment>=======================================</comment>");
		}


		if (!$pushed_files) {
			if (!$silent) {
				debardeur_log("Rien a faire");
			}
			return Command::SUCCESS;
		}

		// classer par ordre chrono
		$pushed_events = [];
		foreach ($pushed_files as $file) {
			$pushed_events[$file] = filemtime($file);
		}
		asort($pushed_events);

		// Notifier partout ?
		$all = !($onlyIRC || $onlyMail);

		if ($onlyIRC or $all) {
			$this->notifier($pushed_events, [$this, 'notifierIrc'], 'irc');
		}
		if ($onlyMail or $all) {
			$this->notifier($pushed_events, [$this, 'notifierMail'], 'mail');
		}

		if (!$noConsume) {
			foreach ($pushed_events as $file => $timestamp) {
				// renommer le fichier traité pour ne pas le rejouer, mais on le garde sous le coude tant qu'on en rodage
				@rename($file, $file . ".sent");
			}

			// purger les sent de plus de 48h
			$delete_time = time() - 48 * 3600;
			foreach ($pushed_files_sent as $file) {
				if (filemtime($file) < $delete_time) {
					@unlink($file);
				}
			}
		}

		return Command::SUCCESS;
	}

	protected function notifier($pushed_events, $callback, $logname = "") {
		include_spip('inc/texte_mini');
		foreach ($pushed_events as $file => $timestamp) {
			if ($this->io->isVerbose()) {
				$this->io->writeln("<info>Traitement</info> <comment>$logname</comment> $file");
			} else {
				debardeur_log("Traitement $logname $file");
			}
			$json = file_get_contents($file);
			if ($data = json_decode($json, true)) {
				$callback($data);
			} else {
				if ($this->io->isVerbose()) {
					$this->io->writeln("<comment>  - fichier invalide</comment>");
				} else {
					debardeur_log(" - fichier invalide");
				}
			}
		}

	}

	protected function notifierIrc($data) {
		// path to Edgard/public/send.php
		if (!defined('_EDGARD_NOTIFIER_IRC') or !_EDGARD_NOTIFIER_IRC) {
			return;
		}

		$repo_name = $data['project']['name'] ?? $data['name'];
		$repo_full_name = $data['path_with_namespace'] ?? $this->removeDomain($data['project']['homepage']);
		$html_url = $data['project']['homepage'] ?? $data['project']['git_http_url'];

		$project = [];
		$title = [];
		$body = [];

		// [fabrique]
		// [fabrique ↪ v3]
		// [fabrique ↪ issue_42]
		// [fork/fabrique ↪ issue_42]
		if (str_starts_with($repo_full_name, 'spip')) {
			$project[] = $repo_name;
		} else {
			$project[] = $repo_full_name;
		}

		// 4 commits
		if (!empty($data['commits'])) {
			$n = count($data['commits']);
			$title[] = $n > 1 ? "$n commits" : "$n commit";
		}

		// Un commentaire de qqc
		if (
			isset($data['object_attributes'], $data['object_kind'])
			&& $data['object_kind'] === 'note'
		) {
			if (!empty($data['merge_request'])) {
				$title[] = '💬 sur ' . $this->ircBold('!' . $data['merge_request']['iid']);
			} elseif (!empty($data['issue'])) {
				$title[] = '💬 sur ' . $this->ircBold('#' . $data['issue']['iid']);
			} else {
				$title[] = '💬';
			}
			$body[] = $data['object_attributes']['url'] . " ▸ " . $this->makeShortLog($data['object_attributes']['description']);
		}

		// Une PR
		elseif (
			isset($data['object_kind'])
			&& $data['object_kind'] === 'merge_request'
		) {
			$pr = $data['object_attributes'];
			$num = $pr['iid'];
			$pr_short = $this->ircBold("!$num");

			if ($pr["action"] === "open") {
				$title[] = "$pr_short créée";
			} elseif ($pr["action"] === "close") {
				$title[] = "$pr_short fermée";
			} elseif ($pr["action"] === "update") {
				$title[] = "$pr_short mise à jour";
			} elseif ($pr["action"] === "reopen") {
				$title[] = "$pr_short réouverte";
			} elseif ($pr["action"] === "merge") {
				$title[] = "$pr_short fusionnée dans " . $pr['target_branch'];
			} elseif ($pr["action"] === "approved") {
				$title[] = "$pr_short revue approuvée";
			} elseif ($pr["action"] === "unapproved") {
				$title[] = "$pr_short revue n’est plus approuvée";
			} elseif ($pr["action"] === "update") {
				if (!empty($data['changes']['title']) or !empty($data['changes']['description'])) {
					$title[] = "$pr_short modifié";
				} elseif (!empty($data['changes']['assignees'])) {
					if (empty($data["changes"]["assignees"]['current'])) {
						$title[] = "$pr_short n'est plus assigné";
					} else {
						$to = array_column($data["changes"]["assignees"]['current'], 'name');
						$title[] = "$pr_short assigné à " . implode(', ', $to);
					}
				} elseif (!empty($data['changes']['labels'])) {
					$labels = array_column($data['object_attributes']['labels'] ?? [], 'title');
					if ($labels) {
						$title[] = "Labels actualisés (" . implode(', ', $labels) . ")";
					} else {
						$title[] = "Label retiré";
					}
				} elseif (!empty($data['changes']['milestone_id'])) {
					if ($pr['milestone_id']) {
						$milestone = $this->milestoneTitle($data['project'], $pr['milestone_id']);
						$title[] = "Jalon affecté pour $milestone";
					} else {
						$title[] = "Jalon retiré";
					}
				} else {
					$title[] = "$pr_short modifié ? " . json_encode($data['changes']);
				}
			}
			// oldies
			elseif ($pr["action"] === "edited") {
				$title[] = "$pr_short modifiée";
			}
			elseif ($pr["action"] === "reviewed") {
				$type = $pr['review']['type'];
				$text = $pr['review']['content'];
				if ($type === 'pull_request_review_approved') {
					$title[] = "$pr_short revue approuvée";
				} elseif ($type === 'pull_request_review_rejected') {
					$title[] = "$pr_short revue rejetée";
				} elseif ($type === 'pull_request_comment') {
				} elseif ($type === 'pull_request_review_comment') {
					$title[] = "$pr_short revue commentée";
				} elseif ($type === 'pull_request_comment') {
					$title[] = "$pr_short commentée";
				} else {
					$title[] = "$pr_short revue $type ?";
				}
				$body[] = $pr['url'] . ($text ? " : " . $this->makeShortLog($text) : "");
			} elseif ($pr["action"] === "label_updated") {
				$labels = array_column($pr['labels'] ?? [], 'name');
				if ($labels) {
					$title[] = "Labels actualisés (" . implode(', ', $labels) . ")";
				} else {
					$title[] = "Label retiré";
				}
			} elseif ($pr["action"] === "milestoned") {
				$milestone = $pr['milestone']['title'] ?? null;
				if ($milestone) {
					$title[] = "Jalon affecté pour $milestone";
				} else {
					$title[] = "Jalon retiré";
				}
			} else {
				$title[] = $pr_short . ' ' . $pr["action"] . " ?";
			}

			if (!in_array($pr['action'], ['reviewed'])) {
				$body[] = $pr['url'] . " : " . $this->makeShortLog($pr['title']);
			}
			unset($pr, $num, $type, $text);
		}

		// Un ticket
		elseif (
			isset($data['object_kind'])
			&& $data['object_kind'] === 'issue'
		) {
			$issue = $data['object_attributes'];
			$num = $issue['iid'];
			$issue_short = $this->ircBold("#$num");
			$action = $issue["action"];

			if ($action === "create") {
				$title[] = "Nouveau ticket";
				// A priori, ça fait created + opened sur les nouveaux tickets
				// On ne mentionne que opened du coup, même si possiblement c’est aussi un ticket réouvert...
				return;
			} elseif ($action === "open") {
				$title[] = "$issue_short ouvert";
			} elseif ($action === "reopen") {
				$title[] = "$issue_short réouvert";
			} elseif ($action === "close") {
				$title[] = "$issue_short fermé";
			} elseif ($action === "update") {
				if (!empty($data['changes']['title']) or !empty($data['changes']['description'])) {
					$title[] = "$issue_short modifié";
				} elseif (!empty($data['changes']['assignees'])) {
					if (empty($data["changes"]["assignees"]['current'])) {
						$title[] = "$issue_short n'est plus assigné";
					} else {
						$to = array_column($data["changes"]["assignees"]['current'], 'name');
						$title[] = "$issue_short assigné à " . implode(', ', $to);
					}
				} elseif (!empty($data['changes']['labels'])) {
					$labels = array_column($data['object_attributes']['labels'] ?? [], 'title');
					if ($labels) {
						$title[] = "Labels actualisés (" . implode(', ', $labels) . ")";
					} else {
						$title[] = "Label retiré";
					}
				} elseif (!empty($data['changes']['milestone_id'])) {
					if ($issue['milestone_id']) {
						$milestone = $this->milestoneTitle($data['project'], $issue['milestone_id']);
						$title[] = "Jalon affecté pour $milestone";
					} else {
						$title[] = "Jalon retiré";
					}
				} elseif (!empty($data['changes']['closed_at'])) {
					if (is_null($data['changes']['closed_at']['previous']) && isset($data['changes']['closed_at']['current'])) {
						return;
					}
				} else {
					$title[] = "$issue_short modifié ? " . json_encode($data['changes']);
				}
			} else {
				$title[] = "$issue_short " . $action . " ?";
			}
			$body[] = $data["object_attributes"]["url"] . " : " . $this->makeShortLog($data["object_attributes"]["title"]);
		}

		elseif (isset($data['release'])) {
			$release = $data['release'];
			$tag = $release['tag_name'];
			$_body = ($release['body'] ? " : " . $this->makeShortLog($release['body']) : "");
			if ($data["action"] === "opened") {
				$title[] = "Release $tag créée";
				$body[] = $release['html_url'] . $_body;
			} elseif ($data["action"] === "edited") {
				$title[] = "Release $tag modifiée";
				$body[] = $release['html_url'] . $_body;
			} elseif ($data["action"] === "deleted") {
				$title[] = "Release $tag supprimée";
			} else {
				$title[] = "Release $tag " . $data["action"] . " ?";
				$body[] = $release['html_url'] . $_body;
			}
			unset($release, $tag, $_body);
		}

		elseif (isset($data['ref']) and strpos($data['ref'], 'refs/heads/') === 0) {
			// sur v2
			$branch = substr($data['ref'], strlen('refs/heads/'));
			if (isset($data["before"]) and $data["before"] === "0000000000000000000000000000000000000000") {
				$project[] = "↪ $branch";
				$title[] = "Nouvelle branche $branch";
				$body[] = $html_url;
			} elseif (isset($data["after"]) and $data["after"] === "0000000000000000000000000000000000000000") {
				$project[] = "↪ $branch";
				$title[] = "Suppression de la branche $branch";
				$body[] = $html_url;
			} elseif (!in_array($branch, ['main', 'master'])) {
				$project[] = "↪ $branch";
			}
		}

		elseif (isset($data['ref']) and strpos($data['ref'], 'refs/tags/') === 0) {
			// nouveau tag v2.0.1
			$tag = substr($data['ref'], strlen('refs/tags/'));
			if (isset($data["before"]) and $data["before"] === "0000000000000000000000000000000000000000") {
				$title[] = "Nouveau tag $tag";
				$body[] = $html_url;
			} elseif (isset($data["after"]) and $data["after"] === "0000000000000000000000000000000000000000") {
				$title[] = "Suppression du tag $tag";
				$body[] = $html_url;
			} else {
				$title[] = "Modification du tag $tag";
				$body[] = $html_url;
			}
		} elseif (
			isset($data["event_name"])
			&& $data["event_name"] === "project_create"
		) {
			$project[] = "🎉";
			$title[] = "Nouveau projet";
			$body[] = $html_url;
		}
		
		
		elseif (
			!empty($data["action"])
			and $data["action"] === "deleted"
			and empty($data['ref'])
		) {
			$project[] = "💀";
			$title[] = "Projet supprimé de {$data["organization"]["login"]}";
		} elseif (!empty($data["forkee"])) {
			$project[] = "⑂";
			$title[] = "Nouveau fork";
			$body[] = $html_url;
			$body[] = "Depuis " . $data["forkee"]["html_url"];
		}

		// par Lui, Elle
		$title[] = $this->findAuthors($data);

		if (!empty($data['commits'])) {
			// repasser en ordre chrono (le plus ancien en premier)
			$max_commits = 3;
			$commits = array_reverse($data['commits']);
			$commits = array_slice($commits, 0, $max_commits);
			foreach ($commits as $commit) {
				$extract = $this->makeShortLog($commit['message']);
				$body[] = substr($commit['url'], 0, -32) . " ▸ $extract";
			}
			if (count($data['commits']) > $max_commits) { 
				$body[] = sprintf('[...]');
			}
		}

		$message = trim("[" . implode(" ", $project) . "] " . implode(" ", $title));
		if (count($body) > 1) {
			$message .= "\n— " . implode("\n— ", $body);
		} elseif ($body) {
			$line = reset($body);
			$message .= " | " . $line;
		}

		if ($this->io->isVerbose()) {
			$this->io->text("[IRC]");
			$this->io->text($message);
			$this->io->writeln("");
		}
		if (!$this->input->getOption('dry-run')) {
			exec('echo ' . escapeshellarg($message) . ' | php ' . _EDGARD_NOTIFIER_IRC);
		}
	}

	protected function milestoneTitle($project, $milestone_id) {
		static $milestones = [];
		$project_id = $project['id'];
		if (isset($milestones[$project_id])) {
			return $milestones[$project_id][$milestone_id]['title'] ?? "milestone id=$milestone_id";
		}
		include_spip('inc/debardeur');
		include_spip('debardeur/connecteur/gitlab');
		$endpoint = gitlab_endpoint_from_url($project['http_url']);
		$url_api = $endpoint . "projects/{$project_id}/milestones";
		$milestones[$project_id] = debardeur_json_recuperer_url($url_api);

		return $milestones[$project_id][$milestone_id]['title'] ?? "milestone id=$milestone_id";
	}

	protected function notifierMail($data) {

		static $envoyer_mail = null;
		if ($envoyer_mail === null) {
			$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
		}

		$dest_zone = "spip-zone-commit@rezo.net";
		$dest_core = "spip-commit@rezo.net";

		//$from = "spip-zone-commit@rezo.net";
		$from = "noreply@spip-contrib.net";

		$dest = $dest_zone;
		$replyto = [ 'spip-dev@rezo.net' => ['nom' => 'Spip-Dev', 'email' => 'spip-dev@rezo.net'] ];
		$from_name = "Git-commit";
		$sujet = $message = "";

		$repo_full_name = $data['path_with_namespace'] ?? $this->removeDomain($data['project']['homepage']);

		$org = explode('/', $repo_full_name, 2)[0] ?? '';
		if ($org === 'spip') {
			$dest = $dest_core;
		}
		$repo_name = $data['repository']['name'] ?? $data['project']['name'] ?? $data['name'];
		$clone_url = $data['repository']['url'] ?? $data['project']['git_http_url'];


		$branch = '';
		if (isset($data['ref']) and strpos($data['ref'], 'refs/heads/') === 0) {
			// sur v2
			$refs = explode('/', $data['ref']);
			$branch = end($refs);
		}

		$nb_commits = (empty($data['commits']) ? 0 : count($data['commits']));

		if ($nb_commits>0) {
			debardeur_log("  - $nb_commits commits sur $clone_url ($branch)");
			$projet = "$repo_name";
			if ($branch && !in_array($branch, ['main', 'master'])) {
				$projet .= " ↪ $branch";
			}
			$messages = [];
			$pre = $repo_full_name;
			if ($nb_commits > 1) {
				$sujet .= " $nb_commits commits";
				$pre  .= " | $nb_commits commits";
			}
			$sujet = "[$projet]$sujet";

			// repasser en ordre chrono (le plus ancien en premier)
			$commits = array_reverse($data['commits']);
			foreach ($commits as $commit) {
				$name = (!empty($commit['author']['name']) ? $commit['author']['name'] : $commit['author']['username']);
				$texte = [];
				$replyto[$commit['author']['email']] = ['nom' => $name, 'email' => $commit['author']['email']];
				$texte[] = "Par $name, le " . affdate_heure($commit['timestamp'])." : \n";
				/*
				$texte[] = "Auteur   : " .$name. " (".$commit['author']['email'].")";
				$texte[] = "Date     : " .affdate_heure($commit['timestamp']) . " (".$commit['timestamp'].")";
				$texte[] = "Log      : ";
				*/
				// passer le log au format citation
				$log = str_replace('\n',"\n",$commit['message']);
				$log = str_replace("\r\n", "\n", $log);
				$log = str_replace("\r", "\n", $log);
				//$log = "> " . str_replace("\n", "\n> ", rtrim($log));
				$texte[] = $log;
				if ($nb_commits == 1) {
					$log = str_replace("\n", " ", $log);
					$log = couper($log, 60);
					$log = html_entity_decode($log);
					$sujet .= " $log";
				}

				$texte[] = '';
				foreach (['added' => '*Ajouté*', 'removed' => '*Supprimé*', 'modified' => '*Modifié*'] as $what => $label) {
					if (count($commit[$what])) {
						$texte[] = $label;
						foreach ($commit[$what] as $modfile) {
							$texte[] = "    $modfile";
						}
					}
				}
				$texte[] = "\nDétails : " .$commit['url'];
				$messages[] = implode("\n", $texte) . "\n";
			}

			$message = "$pre\n-\n".implode("\n==============================\n", $messages);

			// si un seul commiter, on reprends son nom comme from
			if (count($replyto) == 2) {
				$from_name = end($replyto);
				$from_name = $from_name['nom'];
			}
		}
		else {

			if (isset($data['object_kind']) && $data['object_kind'] === 'push') {
				debardeur_log("  - PR sur $clone_url - on ne notifie pas");
			} elseif (isset($data['object_kind']) && $data['object_kind'] === 'note') {
				debardeur_log("  - Commentaire sur $clone_url - on ne notifie pas");
			} if (isset($data['event_name']) && $data['event_name'] === 'project_create') {
				debardeur_log("  - Nouvau projet $clone_url - on ne notifie pas");
			}elseif (!empty($data['refs'])) {
				if (strpos($data['refs'], 'refs/tags/') !== false){
					debardeur_log("  - Tag " . $data['refs'] . " sur $clone_url - on ne notifie pas");
				} else {
					debardeur_log("  - Branche ". $data['refs'] . " sur $clone_url - on ne notifie pas");
				}
			} else {
				debardeur_log("<comment>  - /!\ Evenement inconnu</comment>");
			}

			// notifier la pose d'un tag ou la creation d'une branche ?
			// pour le moment niet, c'est inutilement verbeux
		}

		if ($sujet and $message) {
			$replyto = array_values($replyto);
			if ($this->output->isVerbose()) {
				// il y a déjà un echo par debarder_log ... hum.
			}
			if (!$this->input->getOption('dry-run')) {
				$envoyer_mail($dest, $sujet,
					[
						'from' => $from,
						'nom_envoyeur' => $from_name,
						'repondre_a' => $replyto,
						'texte' => $message,
						'html' => '', // on reste au format texte
					]);
			}
			//debardeur_log("From: $dest");
			//debardeur_log("To: $dest");
			//debardeur_log("Reply-To: $replyto");
			debardeur_log("    Mail pour $dest : $sujet");
			debardeur_log($message);
		} else {
			debardeur_log("    Rien a faire");
		}
		debardeur_log("\n");
	}

	protected function findAuthors($data) {
		$names = $_doublons = [];
		$name = $_names = '';

		if (!empty($data['commits'])) {
			foreach ($data['commits'] as $commit) {
				$_name = $commit['author']['name'];
				if (!in_array(strtolower($_name), $_doublons)) {
					$names[] = $_name;
					$_doublons[] = strtolower($_name);
					$_doublons[] = strtolower($commit['author']['name']);
					$_doublons[] = strtolower($commit['author']['email']);
					$_doublons = array_unique($_doublons);
				}
			}
			$_names = implode(', ', $names);
		}
		if ($_names) {
			$name = "par $_names";
		}

		$sender = $data['user_username'] ?? $data['user_name'] ?? $data['user']['name'] ?? $data['user']['username'] ?? '';
		$sender_email = $data['user_email'] ?? $data['user']['email'] ?? '';
		if ($sender) {
			if (!$name) {
				$name = "par $sender";
			} elseif (
				!in_array(strtolower($sender), $_doublons)
				&& !in_array($sender_email, $_doublons)
			) {
				$name .= " (Envoyé par $sender)";
			}
		}
		return $name;
	}

	protected function makeShortLog($log, $couper = 160) {
		$log = str_replace('\n'," ", $log);
		$log = str_replace("\n"," ", $log);
		$log = str_replace('  '," ", $log);
		$log = trim($log);
		$extract = couper($log, $couper, '…');
		return $extract;
	}

	protected function removeDomain(string $url): string {
		return substr($url, strlen($this->homepage));
	}

	protected function ircBold(string $text): string {
		return "\x02$text\x0F";
	}
}
